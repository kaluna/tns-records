<?php

use Kaluna\Boilerplate\View;

get_header(); ?>

<main class="mdt--xl pdb--xl">

	<section class="not--found tac">

		<div class="container">
			
			<h1 class="text-black">404 not found!</h1>

			<p>Apologies, this page doesn't exist. Please use the navigation to find your way.</p>

			<p>Alternatively, <a href="mailto:hello@kaluna.co.uk" title="Contact support">contact support</a>.</p>

		</div>

	</section>

</main>

<?php get_footer(); ?>