<?php 

use Kaluna\boilerplate\View;

global $post;

$post_type = get_post_type($post);
$post = (new Kaluna\PostModel)->get($post->ID);

if ( $post_type == 'post' ) { ?>

	<div class="col-12 col-sm-6 col-lg-4">

		<?php View::get_partial('collection/detail', $post); ?>

	</div>

<?php } else { ?>

	<div class="col-12 col-sm-6 col-lg-3">

		<?php View::get_partial('collection/index', $post); ?>

	</div>

<?php }