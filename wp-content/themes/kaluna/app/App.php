<?php

namespace Kaluna;

use Symfony\Component\Yaml\Yaml;

class App
{
    private $app_config;
    public $theme_config;

    public function __construct()
    {
        // Load the private app config dynamically
        $this->app_config = $this->load_config('app');

        // Load the public theme config statically.
        $this->theme_config = $this->load_config('theme');

        // Bootstrap the app
        $this->init();
    }

    /**
     * @param string $variable_name
     *
     * @return bool|string
     */
    public static function get_theme_config(string $variable_name)
    {
        $self = new static();
        return array_key_exists($variable_name, $self->theme_config) ? $self->theme_config[$variable_name] : false;
    }

    private function init()
    {
        // Load the app's dependencies that aren't handled by Composer's autoloader.
        new FunctionLoader($this->app_config['autoload_stack']);

        // New up the other class based config.
        new Navigation;
        new Icons;
        new Enqueue;
        new CustomPostTypes;
        new WoocommerceSettings;
        new ThemeSettings;
    }

    /**
     * @param $config
     *
     * @return mixed
     */
    private function load_config(string $config)
    {   
        return Yaml::parseFile($this->core_path() . '/config/' . $config . '.yml');
    }

    /**
     * @return string
     */
    private function core_path(): string
    {
        return __DIR__;
    }

    /**
     * @return string
     */
    private function theme_path(): string
    {
        return trailingslashit(get_template_directory());
    }
}
