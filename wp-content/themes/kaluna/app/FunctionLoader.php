<?php

namespace Kaluna;

class FunctionLoader
{
    public function __construct(array $directories)
    {
        foreach ($directories as $directory)
            foreach (glob(get_template_directory() . '/app/' . $directory . '/*') as $file)
                include_once $file;
    }
}
