<?php

namespace Kaluna;

/**
 * Load front end theme scripts and styles.
 * Don't rename the class or constructor, just add wp_enqueue_x lines and js_dependencies as required.
 * Wrap them in logic if you need to load things, for example, a page by page basis.
 * When the theme bootstraps, this class will fire against Wordpress' wp_enqueue_scripts hook.
 */
class Enqueue
{
    public function __construct()
    {
        
        add_action('wp_enqueue_scripts', [$this, 'assets']);

    }

    public function assets() 
    {
        
        // Theme stylesheet
        wp_enqueue_style('style', get_template_directory_uri() . '/dist/style.css', false, KALUNA_VERSION);

        // Theme JS
        wp_enqueue_script('javascript', get_template_directory_uri() . '/dist/app.min.js', false, KALUNA_VERSION, true);

        remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
        remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
        remove_action( 'admin_print_styles', 'print_emoji_styles' );

        if ((function_exists('is_woocommerce') && !is_woocommerce()) && !is_cart() && !is_checkout() && !is_page('contact'))
            wp_deregister_script('jquery');
    
    }

}

