<?php

namespace Kaluna;

class CustomPostTypes
{
    public function __construct()
    {
        $post_types = [
            'Band' => [
                'public' => true,
                'hierarchical' => true,
                'label' => 'Band',
                'labels' => [
                    'name'  => _x( 'Bands', 'Post Type General Name', 'bsaci' ),
                    'menu_name' => _x( 'Bands', 'Post Type Singular Name', 'bsaci' ),
                    'singular_name' => _x( 'Bands', 'Post Type Singular Name', 'bsaci' ),
                ],
                'supports' => ['title', 'editor', 'thumbnail', 'page-attributes'],
                'has_archive' => true,
                'rewrite' => array('slug' => 'bands'),
                'menu_icon' => 'dashicons-format-audio',
            ]
        ];

        $this->register_custom_post_types($post_types);
    }

    private function register_custom_post_types($post_types)
    {
        foreach ($post_types as $post_type => $args)
            register_post_type($post_type, $args);
    }
}