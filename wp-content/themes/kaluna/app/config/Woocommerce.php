<?php 

namespace Kaluna;

/**
 * WoocommerceSettings
 */
class WoocommerceSettings
{
	
	public function __construct()
	{

		add_action( 'init', [$this, 'createBandTaxonomy'] );
		add_action( 'admin_init', [$this, 'populateBandTaxonomy'] );

	}

	public function createBandTaxonomy()  {

	    $labels = array(
	        'name'                       => 'TNS Bands',
	        'singular_name'              => 'Band',
	        'menu_name'                  => 'Bands',
	        'all_items'                  => 'All Bands',
	        'parent_item'                => 'Parent Band',
	        'parent_item_colon'          => 'Parent Band:',
	        'new_item_name'              => 'New Band Name',
	        'add_new_item'               => 'Add New Band',
	        'edit_item'                  => 'Edit Band',
	        'update_item'                => 'Update Band',
	        'separate_items_with_commas' => 'Separate Band with commas',
	        'search_items'               => 'Search Bands',
	        'add_or_remove_items'        => 'Add or remove Bands',
	        'choose_from_most_used'      => 'Choose from the most used Bands'
	    );

	    $args = array(
	        'labels'                     => $labels,
	        'hierarchical'               => true,
	        'public'                     => true,
	        'show_ui'                    => true,
	        'show_admin_column'          => true,
	        'show_in_nav_menus'          => true,
	        'rewrite'					 => ['slug' => 'product-band']

	    );

	    register_taxonomy( 'product_band', ['product', 'post'], $args );
	    register_taxonomy_for_object_type( 'product_band', 'product', $args );
	    register_taxonomy_for_object_type( 'product_band', 'post', $args );

    }

    public function populateBandTaxonomy() 
    {
    	
    	$args = [

    		'post_type' => 'band',
    		'posts_per_page' => -1

    	];
    	
    	$query = new \WP_Query($args);
    	
    	if ( $query->have_posts() ) :
    	 
    		while ( $query->have_posts() ) : 

    			$query->the_post();

    			global $post;

    			// create new
    			if ( term_exists($post->post_title) ) {

    				$inserted = wp_insert_term($post->post_title, 'product_band', null);

    			}

    		endwhile;
    	 
    		wp_reset_postdata();
    	
    		else :
    	
    	endif;
 	   
    }

}