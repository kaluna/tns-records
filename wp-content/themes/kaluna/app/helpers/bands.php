<?php 

add_action('template_redirect', 'band_alumni_redirect');

function band_alumni_redirect() {

	if ( is_single() && get_post_type() == 'band' && get_field('alumni') == true ) {

		$band_page = get_post_type_archive_link('band');

		if ( wp_redirect($band_page, 301) ) {
		    exit;
		}

	}

}