<?php

function my_deregister_scripts(){

	wp_dequeue_script( 'wp-embed' );

}

add_action( 'wp_footer', 'my_deregister_scripts' );

add_action( 'wp_print_styles', 'wps_deregister_styles', 120 );

function wps_deregister_styles() {

    wp_dequeue_style('wp-block-library');
    wp_dequeue_style('wc-bundle-css');
    wp_dequeue_style('jetpack_css');
    wp_deregister_style('jetpack_css');
    wp_deregister_style('wc-bundle-css');
	wp_deregister_style('AtD_style'); // After the Deadline
	wp_deregister_style('jetpack-carousel'); // Carousel
	wp_deregister_style('grunion.css'); // Grunion contact form
	wp_deregister_style('the-neverending-homepage'); // Infinite Scroll
	wp_deregister_style('infinity-twentyten'); // Infinite Scroll - Twentyten Theme
	wp_deregister_style('infinity-twentyeleven'); // Infinite Scroll - Twentyeleven Theme
	wp_deregister_style('infinity-twentytwelve'); // Infinite Scroll - Twentytwelve Theme
	wp_deregister_style('noticons'); // Notes
	wp_deregister_style('post-by-email'); // Post by Email
	wp_deregister_style('publicize'); // Publicize
	wp_deregister_style('sharedaddy'); // Sharedaddy
	wp_deregister_style('sharing'); // Sharedaddy Sharing
	wp_deregister_style('stats_reports_css'); // Stats
	wp_deregister_style('jetpack-widgets'); // Widgets
	wp_deregister_style('wc-block-style');
	wp_deregister_style('wc-bundle-style');
	wp_deregister_style('wcpb-style');
	wp_deregister_style('angelleye-express-checkout-css');
	wp_deregister_style('wt-smart-coupon');

}

add_filter('jetpack_implode_frontend_css', '__return_false', 99);