<?php 

add_action('wp_head', 'formatFAQs', 20);

function formatFAQs() { ?>

	<?php 

		$faqs = get_field('faqs');
		$faq_json = [];
		$i = 0;

		if ($faqs) { ?>

		<script type="application/ld+json">
			
			{
		      "@context": "https://schema.org",
		      "@type": "FAQPage",
		      "mainEntity": [{
		        "@type": "Question",
		        "name": "<?php echo $faqs[0]['question']; ?>",
		        "acceptedAnswer": {
		          "@type": "Answer",
		          "text": "<?php echo $faqs[0]['answer']; ?>"
		        }
		      }, 
		      
		      <?php foreach ($faqs as $faq) {

					if ( $i !== 0 )	{ ?>
						
					{
				        "@type": "Question",
				        "name": "<?php echo $faq['question']; ?>",
				        "acceptedAnswer": {
				          "@type": "Answer",
				          "text": "<?php echo str_replace('"', '\"', $faq['answer']); ?>"
				        }
				    }<?php echo end($faqs) !== $faq ? ',' : null; ?>

					<?php }

					$i++;

				}; ?>]

		  	}

	    </script>

<?php

	}

}