<?php 

add_filter( 'gform_ajax_spinner_url', 'spinner_url', 10, 2 );

function spinner_url( $image_src, $form ) {

    return get_resource('svgs/spinner.svg');

}

add_filter( 'gform_tabindex', 'change_tabindex' , 10, 2 );

function change_tabindex( $tabindex, $form ) {

    return 10 + $form['id'];

}