<?php 

/**
* auto create image overrides
*/

add_action('init', 'ImageSizes');

function ImageSizes() {

    // remove image sizes that aren't ours

    $remove = ['thumbnail', 'medium', 'medium_large', 'large', '1536x1536', '2048x2048', 'alm-thumbnail', 'alm-cta', 'alm-gallery'];

    foreach ($remove as $item) {
        
        $removed = remove_image_size($item);

    }

    $sizes = get_intermediate_image_sizes();

    foreach ($sizes as $size) {

        // remove loaders, woo & shop
        
        if ( !stristr($size, 'loader') && !stristr($size, 'woocommerce') && !stristr($size, 'shop') && !in_array($size, $remove) ) {
            
            $title = str_replace('_', ' ', $size);

            $fields[] = [
                'key' => 'field_' . $size,
                'label' => ucwords($title),
                'name' => 'field_' . $size,
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'return_format' => 'array',
                'preview_size' => $size,
                'library' => 'all',
                'min_width' => 0,
                'min_height' => 0,
                'min_size' => 0,
                'max_width' => 0,
                'max_height' => 0,
                'max_size' => 0,
                'mime_types' => ''
            ];

        }

    }

    if (function_exists('acf_add_local_field_group')) {
        
        acf_add_local_field_group(array(

            'key' => 'featured_images',
            'title' => 'Featured Images',
            'fields' => $fields,
            'location' => [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'post',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'product',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'page',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'band',
                    ],
                ],
            ],
            'menu_order' => 1,
            'position' => 'side',

        ));
            
    }

}

/**
* getImage
*/

function getImage($id, $size = 'full', $output, $mobile = true, $caption = false){

    $image = null;

    if ( $id && $size ) {

        $image_object = wp_get_attachment_image_src($id, $size);
        $image_object_loader = wp_get_attachment_image_src($id, $size . '-loader');
        $image_object_square = wp_get_attachment_image_src($id, 'square');

        $mobile_override = get_field('mobile', $id);

        if ( $mobile_override )  {

            $image_object_square = wp_get_attachment_image_src($mobile_override['ID'], 'mobile');

        }

        // checks

        $object_check = file_exists(str_replace(get_bloginfo('url'), ABSPATH, $image_object[0]));
        $loader_check = file_exists(str_replace(get_bloginfo('url'), ABSPATH, $image_object_loader[0]));
        $square_check = file_exists(str_replace(get_bloginfo('url'), ABSPATH, $image_object_square[0]));

        if ( $object_check == false ) {

            // if image object isn't found, get big version, and if that still isn't found, we fail
            $object_check = file_exists(str_replace(get_bloginfo('url'), ABSPATH, wp_get_attachment_image_src($id, 'full')[0]));

        }

        if ( $object_check == true ) {

            /**
            * just return image src
            */

            if ( $output == 'src' ) {

                $image = $image_object[0];
                
            }

            /**
            * return image element with alt
            */

            if ( $output == 'html' ) {

                $alt = get_post_meta( $id, '_wp_attachment_image_alt', true );

                if ( $loader_check == false ) {

                    if ( $mobile == true && $square_check !== false ) {

                        $image = '<img class="jsImages" data-mobile="'.$image_object_square[0].'" data-tablet="'.$image_object[0].'" src="'.$image_object[0].'" alt="'.$alt.'">';

                    } else {

                        $image = '<img data-all="'.$image_object[0].'" src="'.$image_object[0].'" alt="'.$alt.'">';

                    }

                } else {

                    if ( $mobile == true && $square_check !== false ) {

                        $image = '<img class="jsImages" data-mobile="'.$image_object_square[0].'" data-tablet="'.$image_object[0].'" src="'.$image_object_loader[0].'" alt="'.$alt.'">';

                    } else {

                        $image = '<img class="jsImages" data-all="'.$image_object[0].'" src="'.$image_object_loader[0].'" alt="'.$alt.'">';

                    }

                }

                if ( $caption == true && wp_get_attachment_caption($id) ) {

                    $caption = wp_get_attachment_caption($id);
                    $image .= '<p class="caption mdb--n">'.$caption.'</p>';

                }

            }

            /**
            * background-image
            */

            if ( $output == 'bg' ) {

                $image = 'data-mobile="'.$image_object_square[0].'" data-tablet="'.$image_object[0].'" style="background-image: url('.$image_object[0].')"';

            }

        }

    }   

    return $image;

}

add_theme_support( 'post-thumbnails' );    
add_image_size( 'loader', 1, 1, ['center', 'center'] ); 
add_image_size( 'loader-full', 1, 1, ['center', 'center'] ); 

add_image_size( 'feature', 1920, 900, ['center', 'center'] ); 
add_image_size( 'feature-loader', 19, 9, ['center', 'center'] ); 

add_image_size( 'mobile', 850   , 850  , ['center', 'center'] ); 
add_image_size( 'mobile-loader', 9, 9, ['center', 'center'] ); 

add_image_size( 'square', 250, 250, ['center', 'center'] ); 
add_image_size( 'square-loader', 6, 6, ['center', 'center'] ); 

add_image_size( 'pair', 750, 750, ['center', 'center'] ); 
add_image_size( 'pair-loader', 6, 7, ['center', 'center'] ); 

add_image_size( 'blog', 800, 550, ['center', 'center'] ); 
add_image_size( 'blog-loader', 8, 5, ['center', 'center'] ); 

