<?php 

function remove_share() {
    remove_filter( 'the_content', 'sharing_display',19 );
    remove_filter( 'the_excerpt', 'sharing_display',19 );
    if ( class_exists( 'Jetpack_Likes' ) ) {
        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
    }
}
  
add_action( 'wp_head', 'remove_share' );
 
function jetpack_share_func( $atts ){
     
    if ( function_exists( 'sharing_display' ) ) {
        sharing_display( '', true );
    }
      
    if ( class_exists( 'Jetpack_Likes' ) ) {
        $custom_likes = new Jetpack_Likes;
        return $custom_likes->post_likes( '' );
    }
 
}
add_shortcode( 'jetpack_share', 'jetpack_share_func' );