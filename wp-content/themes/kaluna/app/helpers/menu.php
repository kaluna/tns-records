<?php

/**
* Add a custom link to the end of a specific menu that uses the wp_nav_menu() function
*/

add_filter('wp_nav_menu_items', 'add_search_link', 10, 2);
add_filter('wp_nav_menu_items', 'add_cart_link', 10, 2);

function add_search_link($items, $args){

    if( $args->theme_location == 'top' || $args->theme_location == 'mobile' ){

        $items .= '<li class="search--item"><form class="search--mini" id="search'.ucfirst($args->theme_location).'" action="'. esc_url( home_url( '/' ) ) .'" method="get"><input class="col mdb--n" type="search" name="s" placeholder="Search"><button id="searchSubmit" type="submit" class="blank col-auto mdb--n"><svg viewBox="0 0 10 10"><use xlink:href="#search" /></use></svg></button></form></li>';

    }

    return $items;

}

function add_cart_link($items, $args){

    global $woocommerce;

    if( $args->theme_location == 'top' || $args->theme_location == 'mobile' ){

        if ( $woocommerce->cart->get_cart() ) {

            $count = count($woocommerce->cart->get_cart());

            if ( $count == 1 ) {

                // check quantity
                foreach ($woocommerce->cart->get_cart() as $item) {
                    
                    $count = $item['quantity'];

                }

            }

            if ( $count > 0 ) {

                $items .= '<li class="wc-cart-link"><a href="'.wc_get_cart_url().'"><svg class="basket" viewBox="0 0 10 10"><use xlink:href="#basket" /></use></svg>  '.$count.'</a></li>';

            }

        }

    }

    return $items;

}