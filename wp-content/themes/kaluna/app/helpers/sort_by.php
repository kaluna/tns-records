<?php 

function sortByCount($a, $b) {

    return $a->count - $b->count;
    
}

function sortByOrder($a, $b) {

    return $a['order'] <=> $b['order'];
    
}
