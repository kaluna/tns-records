<?php

add_filter('upload_mimes', 'allowSVGs');

function allowSVGs($mimes) {

    $mimes['svg'] = 'image/svg+xml';
    return $mimes;

}
