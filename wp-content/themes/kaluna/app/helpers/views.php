<?php

function load_view($view, $stack = null)
{
    $path = '/views/' . $view . '.php';

    if (file_exists(locate_template($path)))
        include(locate_template($path));
    else
        echo $path . ' could not be found.';
}