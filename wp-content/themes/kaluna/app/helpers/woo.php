<?php 

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
* Disable WooCommerce block styles (front-end).
*/

function slug_disable_woocommerce_block_styles() {

	wp_dequeue_style( 'wc-block-style' );

}

add_action( 'wp_enqueue_scripts', 'slug_disable_woocommerce_block_styles' );