<?php 

namespace Kaluna;

/**
 * BandModel
 */
class BandModel
{

	public function getAll($per_page = 999, $query = false) 
	{

		global $band_query;
		$bands = [];

	    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

		$args = [

			'post_type' => 'band',
			'posts_per_page' => $per_page,
	        'paged' => $paged,
	        'nopaging' => false,
	        'orderby' => 'title',
            'order'   => 'asc'

		];

		if ( $query ) {

			$args = array_merge($args, $query);

		}
		
		$band_query = new \WP_Query($args);
		
		if ( $band_query->have_posts() ) :
		 
			while ( $band_query->have_posts() ) :

				$band_query->the_post();

				global $post;

				$bands[] = self::get($post);
		
			endwhile;
		 
			wp_reset_postdata();
		
		endif;

		return $bands;
	
	}

	public static function get($band, $fields = false) 
	{

		$post = [];
		
		if ( !is_array($band) ) {

			$band = get_post($band);

		}

		$post = [

			'id' => $band->ID,
			'name' => $band->post_title,
			'socials' => self::getSocials(),
			'products' => self::getProducts($band->post_name),
			'posts' => self::getPosts($band->post_name),
			'images' => PostModel::getImages($band->ID),
			'url' => get_the_permalink($band->ID)

		];

		if ( $fields == true ) {

			$post = array_merge($post, ['fields' => self::getFields()]);

		}

		return $post;
	
	}

	public static function getSocials() 
	{
		
		$socials = get_field('network');
		$data = [];

		foreach ($socials as $social) {
			
			$data[sanitize_title($social['link']['title'])] = [

				'name' => isset($social['link']['title']) ? $social['link']['title'] : false,
				'slug' => isset($social['link']['title']) ? sanitize_title($social['link']['title']) : false,
				'url' => isset($social['link']['url']) ? $social['link']['url'] : false,
				'target' => isset($social['link']['target']) ? $social['link']['target'] : false,

			];

		}

		return $data;
	
	}

	public static function getProducts($slug) 
	{

		$products = [];
		
		$args = [

			'post_type' => 'product',
			'posts_per_page' => -1,
	        'meta_query' => [
                [
                    'key' => '_stock_status',
                    'value' => 'instock'
                ]
            ],
	        'tax_query' => [
	        	'relation' => 'AND',
                [
	                'taxonomy' => 'product_visibility',
			        'field'    => 'name',
			        'terms'     => ['exclude-from-catalog'],
			        'operator' => 'NOT IN',
                ],
                [

                	'taxonomy' => 'product_band',
                	'field' => 'slug', 
                	'terms' => $slug

                ]
            ]

		];	
		
		$query = new \WP_Query($args);
		
		if ( $query->have_posts() ) :
		 
			while ( $query->have_posts() ) : 

				$query->the_post();

				global $post;

				$products[] = (new ProductModel)->get($post->ID);
		
			endwhile;
		 
			wp_reset_postdata();
		
		endif;

		return $products;
	
	}

	public static function getPosts($slug) 
	{

		$posts = [];

		$args = [

			'post_type' => 'post',
			'posts_per_page' => 3,
			'tax_query' => [

				[

					'taxonomy' => 'product_band',
					'field' => 'slug', 
					'terms' => $slug,

				]

			]

		];	
		
		$query = new \WP_Query($args);
		
		if ( $query->have_posts() ) :
		 
			while ( $query->have_posts() ) : 

				$query->the_post();

				global $post;

				$posts[] = (new PostModel)->get($post->ID);
		
			endwhile;
		 
			wp_reset_postdata();
		
		endif;

		return $posts;
	
	}

	public static function getFields() 
	{
		
		$fields = [];

		$object = ['top_quote', 'introduction', 'biography', 'biography_image', 'for_fans_of', 'alternative_content', 'videos', 'bandcamp_player'];

		foreach ($object as $item) {

			$field = get_field($item);

			if ( $item == 'biography_image' ) {

				$fields[$item] = isset($field['ID']) ? getImage($field['ID'], 'full', 'html', false) : false;

			} else if ( is_array($field) ) {

				$fields[$item] = $field;

			} else {

				$fields[$item] = make_clickable($field);

			}

		}		

		return $fields;
	
	}	

}
