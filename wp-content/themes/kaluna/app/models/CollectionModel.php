<?php 

namespace Kaluna;

/**
 * CollectionModel
 */
class CollectionModel
{

	public function get($post_type, $type, $query = false, $amount = 3) 
	{

		$items = [];
		
		$args = [

			'post_type' => $post_type,
			'posts_per_page' => $amount,
			'orderby' => $type,
			'order' => 'DESC'

		];

		if ( $post_type == 'product' ) {

			$args = array_merge($args, [

				'tax_query' => [

	        		'relation' => 'AND',

					[

						'taxonomy' => 'product_visibility',
						'field'    => 'name',
						'terms'     => ['exclude-from-catalog'],
						'operator' => 'NOT IN',

					]

				]

    	    ]);

		}

		if ( $query && isset($args['tax_query']) ) {

			$args['tax_query'][] = $query;

		}

		$query = new \WP_Query($args);
		
		if ( $query->have_posts() ) :
		 
			while ( $query->have_posts() ) : 

				$query->the_post();

				global $post;

				$items[] = (new PostModel)->get($post);

			endwhile;
		 
			wp_reset_postdata();
		
		endif;

		return $items;
	
	}

}