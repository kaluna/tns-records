<?php 

namespace Kaluna;
use Kaluna\ProductModel;
use Kaluna\BlogModel;
use Kaluna\ClassesModel;

class ComponentModel
{

	public function exists($post_id) {

		$status = false;

		if ( function_exists('have_rows') && have_rows('components', $post_id) ) {

			$status = true;

		}

		return $status;

	}

	public function get($post_id) {

		if ( function_exists('have_rows') ) {

			$components = [];

			if( have_rows('components', $post_id) ):

			    while ( have_rows('components', $post_id) ) : the_row();

			    	if ( get_row_layout() == 'hero' ) {

			    		$data = $this->getHero();
					
					}

			    	if ( get_row_layout() == 'collection' ) {

			    		$data = $this->getCollection();
					
					}

			    	if ( get_row_layout() == 'pair' ) {

			    		$data = $this->getPair();
					
					}

			    	if ( get_row_layout() == 'triplet' ) {

			    		$data = $this->getTriplet();
					
					}

			    	if ( get_row_layout() == 'statement' ) {

			    		$data = $this->getStatement();
					
					}

			    	if ( get_row_layout() == 'blog' ) {

			    		$data = $this->getBlog();
					
					}

			    	if ( get_row_layout() == 'textarea' ) {

			    		$data = $this->getTextarea();
					
					}

			    	if ( get_row_layout() == 'signup' ) {

			    		$data = $this->getSignup();
					
					}

			    	if ( get_row_layout() == 'form' ) {

			    		$data = $this->getForm();
					
					}

			    	if ( get_row_layout() == 'resources' ) {

			    		$data = $this->getResources();
					
					}

			    	$components[] = [

			    		'layout' => get_row_layout(),
			    		'data' => $data

			    	];

			    endwhile;

			endif;

			return $components;

		}

	}

	public function getHero() 
	{
		
		$data = [

			'title' => get_sub_field('title'),
			'text_color' => get_sub_field('text_color'),
			'sub_title' => get_sub_field('sub_title'),
			'link' => get_sub_field('link'),
			'text_position' => get_sub_field('text_position'),
			'image' => get_sub_field('image') ? getImage(get_sub_field('image')['ID'], 'feature', 'html', true) : false,

		];

		return $data;
	
	}

	public function getStatement() 
	{
		
		$data = [

			'image' => get_sub_field('image') ? getImage(get_sub_field('image')['ID'], 'full', 'html', false) : false,
			'content' => get_sub_field('content'),

		];

		return $data;
		
	
	}

	public function getBlog() 
	{

		$posts = [];
		$i = 0;

		if ( get_sub_field('type') == 'random' ) {

			$posts = (new PostModel)->getAll(get_sub_field('amount'), 'rand'); 

		}

		if ( get_sub_field('type') == 'latest' ) {

			$posts = (new PostModel)->getAll(get_sub_field('amount')); 

		}

		if ( get_sub_field('type') == 'custom' ) {

			$posts = [];

			foreach (get_sub_field('items') as $item) {

				$posts[] = (new PostModel)->get($item);

				// stop at 4

				if ( $i == 3 ) {

					break;

				}

				$i++;

			}
			
		}

		return [

			'title' => get_sub_field('title'),
			'count' => count($posts),
			'posts' => $posts,
			'page_for_posts' => get_permalink(get_option('page_for_posts')),

		];

	}

	public function getPair() 
	{
		
		$tears = self::getTears(get_sub_field('tears'));
		
		$data = [

			'content' => get_sub_field('content'),
			'image' => get_sub_field('image') ? getImage(get_sub_field('image')['ID'], 'pair', 'html', false) : false,
			'link' => get_sub_field('link'),
			'column_alignment' => get_sub_field('column_alignment'),
			'background' => get_sub_field('background'),
			'grunge' => get_sub_field('grunge'),
			'tears' => $tears

		];

		return $data;
	
	}

	public function getTriplet() 
	{

		$items = [];

		if ( get_sub_field('items') ) {

			foreach (get_sub_field('items') as $item) {
				
				$items[] = [

					'content' => $item['content'],
					'image' => $item['image'] ? getImage($item['image']['ID'], 'pair', 'html', false) : false,

				];

			}

		}
		
		$data = [

			'title' => get_sub_field('title'),
			'items' => $items,

		];

		return $data;
	
	}

	public function getTextarea() 
	{
		
		$data = [

			'content' => get_sub_field('content'),
			'stroke' => get_sub_field('stroke')

		];

		return $data;
	
	}

	public function getCollection() 
	{

		$query = false;

		if (get_sub_field('items')) {

			$items = [];

			foreach (get_sub_field('items') as $item) {
				
				$item = get_post($item);
				$items[] = (new PostModel)->get($item);

			}
			
		} else {

			$type = get_sub_field('type');

			if ($type == 'latest'): $type = 'publish_date'; endif;
			if ($type == 'random'): $type = 'rand'; endif;

			if ( $type == 'taxonomy' && get_sub_field('post_type') == 'post' ) {

				$type = 'date';
				$query['cat'] = get_sub_field('taxonomy');

			}

			if ( $type == 'taxonomy' && get_sub_field('post_type') == 'product' ) {

				$type = 'date';

				$query = [

					'taxonomy' => 'product_cat',
					'field' => 'term_id',
					'terms' => get_sub_field('product_taxonomy')

				];

			}

			$items = (new CollectionModel)->get(get_sub_field('post_type'), $type, $query, get_sub_field('amount') !== '' ? get_sub_field('amount') : 3);

		}

		// tears

		$tears = self::getTears(get_sub_field('tears'));

		// link

		$link = get_sub_field('link');

		if ( get_sub_field('background') == 'bg-pink' && isset($link) ) {

			$link['type'] = 'black';

		}

		$data = [
			
			'title' => get_sub_field('title'),
			'link' => get_sub_field('link'),
			'background' => get_sub_field('background'),
			'grunge' => get_sub_field('grunge'),
			'link' => $link,
			'tears' => $tears,
			'items' => $items,
			'count' => count($items)

		];
		
		return $data;
	
	}

	public function getSignup() 
	{
	

		$data = [

			'title' => get_sub_field('title'),
			'description' => get_sub_field('description'),
			'form_id' => get_sub_field('form_id'),

		];

		return $data;
	
	}

	public function getForm() 
	{

		$data = [

			'title' => get_sub_field('title'),
			'introduction' => get_sub_field('introduction'),
			'form_id' => get_sub_field('form_id'),
			'background' => get_sub_field('background'),
			'grunge' => get_sub_field('grunge'),
			'tears' => self::getTears(get_sub_field('tears')),
			'tabindex' => get_sub_field('form_id') !== null ? 27 + get_sub_field('form_id') : 27

		];

		return $data;
	
	}

	public function getResources() 
	{
		
		$sections = get_sub_field('section');
		return $sections;
	
	}

	public static function getTears($tears_object) 
	{

		$tears = false;
		
		if ( $tears_object['direction'] ) {

			foreach ($tears_object['direction'] as $item) {
				
				$tears[] = 'tear--' . $tears_object['colour'] . ucfirst($item);

			}

			$tears = implode(' ', $tears);

		}

		return $tears;
	
	}

}