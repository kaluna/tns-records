<?php 

namespace Kaluna;

class PageModel
{

	public static function get($post) 
	{	

		$date = get_the_date('F jS, Y', $post);

		$post = [

        	'id' => $post->ID,
        	'title' => $post->post_title,
        	'images' => self::getImages($post),
        	'excerpt' => apply_filters('the_content', wp_trim_words($post->post_content, 15)),
        	'content' => apply_filters('the_content', $post->post_content),
        	'posted_on' => $date,
        	'url' => get_the_permalink($post->ID),
        	'link' => [

        		'title' => 'Read more',
        		'url' => get_the_permalink($post->ID),
        		'type' => 'grey',
        		'target' => '_self'

        	]

        ];

		return $post;

	}

	public static function getTemplateSlug($post) 
	{
		
		return get_page_template_slug($post);
	
	}

	public static function getAll($amount = 4) 
	{

        global $query;

	    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

		$args = [

			'post_type' => 'post', 
			'posts_per_page' => $amount,
	        'paged' => $paged,
	        'nopaging' => false

		];
		
		$query = new \WP_Query( $args );
		
		if ( $query->have_posts() ) {

		    while ( $query->have_posts() ) {

		        $query->the_post();

		        global $post;

		        $data[] = self::get($post);

		    }

		    wp_reset_postdata();

		}

		return $data;

	}

	public static function getImages($id) 
	{
		
	    $sizes = get_intermediate_image_sizes();
		$remove = ['medium', 'medium_large', 'large', '1536x1536', '2048x2048'];

	    foreach ($sizes as $size) {

	    	/**
	    	* see main thumbnail has the relevant chops, but if acf has been used - use that.
	    	*/
	    	
	        if ( !stristr($size, 'loader') && !stristr($size, 'woocommerce') && !stristr($size, 'shop') && !in_array($size, $remove) ) {
	        	
	        	$data[$size] = [

	        		'html' => get_post_thumbnail_id($id) ? getImage(get_post_thumbnail_id($id), $size, 'html', true) : false,
	        		'src' => get_post_thumbnail_id($id) ? getImage(get_post_thumbnail_id($id), $size, 'src', true) : false

	        	];

	        	$image = get_field('field_' . $size, $id);

	        	if ( $image !== false ) {

		        	$data[$size] = [

		        		'html' => isset($image['ID']) ? getImage($image['ID'], $size, 'html', true) : false,
						'src' => isset($image['ID']) ? getImage($image['ID'], $size, 'src', true) : false

		        	];

	        	}

	        	if ( $data[$size]['src'] == false && $data[$size]['html'] == false ) {

	        		$data[$size] = [

	        			'html' => '<img src="'.get_resource('jpg/placeholder-' . $size . '.jpg').'" alt="">',
	        			'src' => get_resource('jpg/placeholder-' . $size . '.jpg')

	        		];

	        	}

		    }

		}

		return $data;

	}

}
