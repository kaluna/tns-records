<?php 

namespace Kaluna;

class PostModel
{

	public static function get($post) 
	{	

		if ( !is_array($post) ) {

			$post = get_post($post);

		}

		$date = get_the_date('F jS, Y', $post);

		$post = [

        	'id' => $post->ID,
        	'name' => $post->post_title,
        	'images' => self::getImages($post),
        	'excerpt' => apply_filters('the_content', wp_trim_words($post->post_content, 15)),
        	'content' => apply_filters('the_content', $post->post_content),
        	'posted_on' => $date,
        	'category' => self::getCategory($post->ID),
        	'categories' => self::getCategories($post->ID),
        	'url' => get_the_permalink($post->ID),
        	'link' => [

        		'title' => 'Read more',
        		'url' => get_the_permalink($post->ID),
        		'type' => 'grey',
        		'target' => '_self'

        	]

        ];

        if ( get_post_type($post['id']) == 'product' ) {
        	
        	$product = wc_get_product($post['id']);

        	if ( $product->get_price_html() ) {
        		
        		$post['price'] = $product->get_price_html();

        	}

        	$terms = wp_get_post_terms($post['id'], 'product_cat');
        	$obj = [];

        	foreach ($terms as $term) {
        		
        		$obj[] = $term->slug;

        	}

        	if ( $product->is_type('bundle') ) {

        		$post['link']['text'] = 'View bundle';

        	}

        	if ( in_array('name-your-price', $obj) == true ) {

        		$post['link']['text'] = 'Name your price';

        	}

        }

		return $post;

	}

	public static function getCategory($id) 
	{
		
		$category = false;

		$categories = get_the_category($id);

		if ( $categories ) {

			$category = [

				'name' => $categories[0]->name,
				'url' => get_term_link($categories[0]->term_id, 'category'),

			];

		}

		return $category;
	
	}

	public static function getCategories($id) 
	{

		$categories = get_the_terms($id, 'category');
		$items = [];

		if ( !empty($categories) ) { 

			foreach ($categories as $category) {
				
				$items[] = [

					'name' => $categories[0]->name,
					'url' => get_term_link($categories[0]->term_id, 'category'),

				];

			};

		}

		return $items;
	
	}

	public static function getSlug() 
	{

		global $woocommerce;
		
		$item = get_queried_object();
		$name = null;

		if (isset($item->post_name)) {
			
			$name = $item->post_name;

		}

		if ( function_exists('get_cart') ) {

			if ( $woocommerce->cart->get_cart() ) {

				$count = count($woocommerce->cart->get_cart());

				if ( $count > 0 ) {

					return $name . ' cart-active';

				}

			} else {

				return $name;

			}

		}
		
	}

	public static function getAll($amount = 4, $order = null) 
	{

        global $query;

	    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

		$args = [

			'post_type' => 'post', 
			'posts_per_page' => $amount,
	        'paged' => $paged,
	        'nopaging' => false

		];

		if ( $order != null ) {

			$args['orderby'] = $order;
			$args['order'] = 'asc';

		}	
		
		$query = new \WP_Query( $args );
		
		if ( $query->have_posts() ) {

		    while ( $query->have_posts() ) {

		        $query->the_post();

		        global $post;

		        $data[] = self::get($post);

		    }

		    wp_reset_postdata();

		}

		return $data;

	}

	public static function getImages($id) 
	{
		
	    $sizes = get_intermediate_image_sizes();
		$remove = ['medium', 'medium_large', 'large', '1536x1536', '2048x2048'];

		if ( is_array($id) ) {

			$id = $id->ID;

		}

	    foreach ($sizes as $size) {

	    	/**
	    	* see main thumbnail has the relevant chops, but if acf has been used - use that.
	    	*/
	    	
	        if ( !stristr($size, 'loader') && !stristr($size, 'woocommerce') && !stristr($size, 'shop') && !in_array($size, $remove) ) {
	        	
	        	$data[$size] = [

	        		'html' => get_post_thumbnail_id($id) ? getImage(get_post_thumbnail_id($id), $size, 'html', true) : false,
	        		'src' => get_post_thumbnail_id($id) ? getImage(get_post_thumbnail_id($id), $size, 'src', true) : false

	        	];

	        	if ( get_field('field_' . $size, $id) ) {

		        	$image = get_field('field_' . $size, $id);

		        	if ( $image !== false ) {

			        	$data[$size] = [

			        		'html' => isset($image['ID']) ? getImage($image['ID'], $size, 'html', true) : false,
							'src' => isset($image['ID']) ? getImage($image['ID'], $size, 'src', true) : false

			        	];

		        	}

	        	}

	        	if ( $data[$size]['html'] == false ) {

		        	if ( $data[$size]['src'] == false && $data[$size]['html'] == false ) {

		        		$data[$size] = [

		        			'html' => '<img src="'.get_resource('placeholders/placeholder-' . $size . '.jpg').'" alt="">',
		        			'src' => get_resource('placeholders/placeholder-' . $size . '.jpg')

		        		];

		        	}

	        	}

		    }

		}

		return $data;

	}

}
