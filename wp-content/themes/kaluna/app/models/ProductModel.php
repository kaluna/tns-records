<?php 

namespace Kaluna;

class ProductModel
{

	public static function get($post) 
	{	

		if ( !is_array($post) ) {

			$post = get_post($post);

		}

		$date = get_the_date('F jS, Y', $post);
		$product = wc_get_product($post);

		$post = [

        	'id' => $post->ID,
        	'name' => $post->post_title,
        	'images' => self::getImages($post),
        	'excerpt' => apply_filters('the_content', wp_trim_words($post->post_content, 15)),
        	'content' => apply_filters('the_content', $post->post_content),
        	'posted_on' => $date,
        	'url' => get_the_permalink($post->ID),
        	'sales' => get_post_meta($post->ID, 'total_sales', true),
        	'price' => $product->get_price_html(),
        	'link' => [

        		'title' => 'Read more',
        		'url' => get_the_permalink($post->ID),
        		'type' => 'grey',
        		'target' => '_self'

        	]

        ];

		return $post;

	}

	public static function getAll($amount = 18, $query = false, $best_sellers = false, $date_query = false) 
	{

        global $product_query;
        $data = [];

	    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

		$args = [

			'post_type' => 'product', 
			'posts_per_page' => $amount,
	        'paged' => $paged,
	        'nopaging' => false,
	        'post_status' => 'publish',
	        'orderby' => 'date',
	        'order' => 'desc',
	        'meta_query' => [
                [
                    'key' => '_stock_status',
                    'value' => 'instock'
                ]
            ],
	        'tax_query' => [
	        	'relation' => 'AND',
                [
	                'taxonomy' => 'product_visibility',
			        'field'    => 'name',
			        'terms'     => ['exclude-from-catalog'],
			        'operator' => 'NOT IN',
                ]
            ]

		];

		if ( $query ) {

			$args['tax_query'][] = $query;

		}

		if ( $date_query ) {

			$args = array_merge($args, $date_query);

		}

		if ( $best_sellers == true ) {

			$args = array_merge($args, [

				'meta_key' => 'total_sales',
				'orderby' => 'meta_value_num'

			]);

		}

		$product_query = new \WP_Query( $args );

		if ( $product_query->have_posts() ) {

		    while ( $product_query->have_posts() ) {

		        $product_query->the_post();

		        global $post;

		        $data[] = self::get($post);

		    }

		    wp_reset_postdata();

		}

		return $data;

	}

	public static function getImages($id) 
	{
		
	    $sizes = get_intermediate_image_sizes();
		$remove = ['medium', 'medium_large', 'large', '1536x1536', '2048x2048'];

	    foreach ($sizes as $size) {

	    	/**
	    	* see main thumbnail has the relevant chops, but if acf has been used - use that.
	    	*/
	    	
	        if ( !stristr($size, 'loader') && !stristr($size, 'woocommerce') && !stristr($size, 'shop') && !in_array($size, $remove) ) {
	        	
	        	$data[$size] = [

	        		'html' => get_post_thumbnail_id($id) ? getImage(get_post_thumbnail_id($id), $size, 'html', true) : false,
	        		'src' => get_post_thumbnail_id($id) ? getImage(get_post_thumbnail_id($id), $size, 'src', true) : false

	        	];

	        	if ( $data[$size]['html'] == false ) {

		        	$image = get_field('field_' . $size, $id);

		        	if ( $image !== false ) {

			        	$data[$size] = [

			        		'html' => isset($image['ID']) ? getImage($image['ID'], $size, 'html', true) : false,
							'src' => isset($image['ID']) ? getImage($image['ID'], $size, 'src', true) : false

			        	];

		        	}

		        	if ( $data[$size]['src'] == false && $data[$size]['html'] == false ) {

		        		$data[$size] = [

		        			'html' => '<img src="'.get_resource('placeholders/placeholder-' . $size . '.jpg').'" alt="">',
		        			'src' => get_resource('placeholders/placeholder-' . $size . '.jpg')

		        		];

		        	}

	        	}

		    }

		}

		return $data;

	}

}
