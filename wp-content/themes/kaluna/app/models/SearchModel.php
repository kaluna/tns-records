<?php 

namespace Kaluna;

/**
 * SearchModel
 */
class SearchModel
{

	public function get($query) 
	{

		global $search_query;
		$results = [];
		
		$args = [

			's' => $query

		];
		
		$search_query = new \WP_Query($args);
		
		if ( $search_query->have_posts() ) :
		 
			while ( $search_query->have_posts() ) : 

				$search_query->the_post();

				global $post;

				$results[get_post_type($post->ID)][] = (new PostModel)->get($post->ID);
		
			endwhile;
		 
			wp_reset_postdata();
		
			else :
		
		endif;

		return $results;
	
	}

}