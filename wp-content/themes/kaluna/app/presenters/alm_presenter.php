<?php 

use Kaluna\boilerplate\View;

function get_alm_presenter($stack)
{

    if ( isset($stack['query']) ) {

    	$post_type = get_post_type();

    	$args = array(

            'post_type'                     => $stack['query']->query['post_type'],
            'posts_per_page'                => $stack['amount'],
            'scroll'                        => 'false',
            'button_label'                  => 'Load more',
            'button_loading_label'          => 'Loading results...',
            'no_result_text'                => 'Sorry, no results found',
            'transition_container_classes'  => 'row',

        );

        if ( $stack['query']->query['post_type'] == 'product' ) {

            $args = array_merge($args, [

                'taxonomy' => 'product_visibility',
                'taxonomy_terms' => 'exclude-from-catalog',
                'taxonomy_operator' => 'NOT IN',
                'taxonomy_relation' => 'AND'

            ]);

        }

        if ( isset($_GET['sortBy']) ) {

            $sortBy = [];

            if ( $_GET['sortBy'] == 'price_asc' ) {

                $sortBy = [

                    'meta_key' => '_price:_stock_status',
                    'meta_value' => ":outofstock",
                    'meta_relation' => "AND",
                    'meta_compare' => "IN:!=",
                    'order' => 'asc',

                ];

            }

            if ( $_GET['sortBy'] == 'price_desc' ) {

                $sortBy = [

                    'meta_key' => '_price:_stock_status',
                    'meta_value' => ":outofstock",
                    'meta_relation' => "AND",
                    'meta_compare' => "IN:!=",
                    'order' => 'desc',

                ];

            }

            if ( !empty($sortBy) ) {

                $sortBy['orderby'] = 'meta_value_num';

                $args = array_merge($args, $sortBy);

            }

        }

        View::get_partial('global/alm', $args);

    }

}