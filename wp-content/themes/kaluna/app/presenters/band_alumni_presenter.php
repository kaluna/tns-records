<?php 

use Kaluna\boilerplate\View;

function get_band_alumni_presenter() {

    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    if ( $paged == 1 ) {

		$stack['title'] = get_page_by_path( 'bands', ARRAY_A, 'page' ) ? get_page_by_path( 'bands', ARRAY_A, 'page' )['post_title'] : null;
		$stack['description'] = get_page_by_path( 'bands', ARRAY_A, 'page' ) ? get_page_by_path( 'bands', ARRAY_A, 'page' )['post_content'] : null;

	}
	
	$query['meta_key'] = 'alumni';
	$query['meta_value'] = 1;
	$query['meta_compare'] = '=';

	$stack['bands'] = (new Kaluna\BandModel)->getAll(999, $query);

	View::get_partial('bands/alumni', $stack);

}