<?php 

use Kaluna\BandModel;
use Kaluna\Boilerplate\View;

function get_band_page_presenter() {

	global $post;
	$band = (new BandModel)->get($post->ID, true);

	// header
	View::get_partial('bands/hero', ['title' => $band['name'], 'image' => $band['images']['feature']['html']]);

	// introduction
	$textarea = [

		'title' => strip_tags($band['fields']['top_quote']),
		'content' => $band['fields']['introduction'], 
		'stroke' => true

	];

	// textarea

	View::get_partial('bands/textarea', $textarea);

	$pair = [

		'title' => 'Biography',
		'content' => $band['fields']['biography'],
		'image' => $band['fields']['biography_image'],
		'alignment' => 'alternative',
		'link' => null,
		'bandcamp' => [

			'link' => $band['socials']['bandcamp'],
			'player' => $band['fields']['bandcamp_player']

		]

	];

	View::get_partial('bands/pair', $pair);

	// alternative_content	
	$alternative_content = [

		'title' => 'Reviews',
		'content' => $band['fields']['alternative_content'],
		'socials' => $band['socials']

	];

	View::get_partial('bands/alternative_content', $alternative_content);

	// videos

	if ( $band['fields']['videos'] ) {

		View::get_partial('bands/videos', $band['fields']['videos']);

	}

	// related products

	if ( $band['products'] ) {

		$count = count($band['products']);

		// limit to 4
		array_splice($band['products'], 4);

		$collection = [

			'title' => 'MERCHANDISE',
			'tears' => 'tear--blackUp tear--blackDown',
			'background' => 'bg-black',
			'grunge' => 'grunge--white',
			'items' => $band['products']

		];

		// find band woo cat and push link

		$term = get_term_by('slug', sanitize_title($band['name']), 'product_band', 'ARRAY_A');

		if ( $term && $count > 4 ) {

			$collection['link'] = [

				'title' => 'More by ' . $band['name'],
				'url' => get_term_link($term['term_id'], 'product_band')

			];

		} else {

			// get "top" category and link to there

			$collection['link'] = [

				'title' => 'All products',
				'url' => get_permalink(wc_get_page_id('shop'))

			];

		}

		View::get_partial('components/collection', $collection);

	}

	// related posts

	if ( $band['posts'] ) {

		$blog = [

			'title' => 'RELATED POSTS',
			'posts' => $band['posts']

		];

		View::get_partial('components/blog', $blog);

	}

};