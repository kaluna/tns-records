<?php 

use Kaluna\boilerplate\View;

function get_best_sellers_presenter() 
{
	
	$stack['title'] = 'Best sellers';

	$query['date_query'] = [

        'after'     => date('Y-m-d', strtotime('-6 Months')),
        'before'    => date('Y-m-d'),
        'inclusive' => true,

	];

	$stack['items'] = Kaluna\ProductModel::getAll(5, false, true, $query);

	View::get_partial('woo/list', $stack);

}