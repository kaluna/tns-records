<?php 

use Kaluna\ComponentModel;
use Kaluna\Boilerplate\View;

function get_components_presenter($post_id = null) {

	$components = (new ComponentModel)->get($post_id);
	
	foreach ($components as $component) {
		
		View::get_partial('components/' . $component['layout'], $component['data']);

	}

}