<?php 

use Kaluna\boilerplate\View;

function get_faqs_presenter() {

	$faqs = get_field('faqs');

	$data = [

		'title' => get_the_title(),
		'content' => get_the_content()

	];

	View::get_partial('layouts/content', $data);

	View::get_partial('layouts/faqs', $faqs);

}