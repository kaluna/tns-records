<?php 

use Kaluna\boilerplate\View;

function get_featured_products_presenter() {

	$stack['title'] = 'Featured products';

	$query = [

		'taxonomy' => 'product_visibility',
	    'field'    => 'name',
	    'terms'    => ['featured'],
	    'operator' => 'IN',

	];

	$stack['items'] = Kaluna\ProductModel::getAll(5, $query);

	View::get_partial('woo/list', $stack);

}