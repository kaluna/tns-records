<?php 

use Kaluna\boilerplate\View;

function get_history_presenter() {

	$fields = get_field('history');
	$ids = [];

	foreach ($fields as $field) {

		if ( $field['id'] ) 

			$ids[] = [

				'title' => $field['id'],
				'slug' => sanitize_title($field['id'])

			];

	}

	View::get_partial('history/navigation', $ids);

	foreach ($fields as $field) {

		$data = $field;

		if ( isset($field['tears']) ) {

			$data['tears'] = Kaluna\ComponentModel::getTears($field['tears']);

		}

		if ( $field['id'] ) {

			$data['id'] = sanitize_title($field['id']);

		}

		if ( $data !== [] )
			View::get_partial('history/' . $field['acf_fc_layout'], $data);
	
	}

}