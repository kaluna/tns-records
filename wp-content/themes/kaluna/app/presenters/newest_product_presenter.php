<?php 

use Kaluna\boilerplate\View;

function get_latest_product_presenter() {

	$stack['title'] = 'Latest products';
	$stack['items'] = Kaluna\ProductModel::getAll(5, ['orderby' => 'date', 'order' => 'desc'], false);

	View::get_partial('woo/list', $stack);


}