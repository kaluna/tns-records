<?php 

use Kaluna\Boilerplate\View;

function get_page_presenter() {

	global $post;
	$rendered = false;

	/**
	* blog
	*/

	if ( is_single() && get_post_type() == 'post' ) {

		$stack = (new Kaluna\PostModel)->get($post);
		View::get_partial('collection/single', $stack);
		$rendered = true;

	}

	if ( is_blog_page() ) {

		global $query;

		$stack = [];

		$stack['title'] = get_the_title(get_option('page_for_posts'));
		$stack['posts'] = (new Kaluna\PostModel)->getAll(get_option('posts_per_page'));
		$stack['query'] = $query;
		$stack['alm'] = get_field('infinite_scroll', 'option') !== null && get_field('infinite_scroll', 'option') == true ? true : false;
		$stack['amount'] = get_field('post_count', 'option') !== null ? get_field('post_count', 'option') : 3;

		View::get_partial('collection/archive', $stack);
		$rendered = true;

	}

	if ( is_search() ) {

		View::get_partial('layouts/search');
		$rendered = true;

	}

	/**
	* woocommerce
	*/

	if ( is_shop() ) {

		global $product_query;

		$products = (new Kaluna\ProductModel)->getAll();

		$stack['archive_title'] = $post->post_title;
		$stack['pagination'] = true;
		$stack['query'] = $product_query;
		$stack['items'] = $products;

		$stack['background'] = 'bg-black';
		$stack['grunge'] = 'grunge--white';
		$stack['tears'] = 'tear tear--blackUp tear--blackDown';

		$stack['filters'] = true;
		$stack['count'] = count($products);
		$stack['alm'] = get_field('infinite_scroll', 'option') !== null && get_field('infinite_scroll', 'option') == true ? true : false;
		$stack['amount'] = get_field('product_count', 'option') !== null ? get_field('product_count', 'option') : 3;

		View::get_partial('woo/archive', $stack);
		$rendered = true;

	}

	if ( is_product() ) {

		wc_get_template_part( 'content', 'single-product' );
		$rendered = true;

	}

	if ( is_cart() || is_checkout() || is_account_page() ) {

		$data = [

			'title' => get_the_title(),
			'content' => get_the_content()

		];
		
		View::get_partial('layouts/content', $data);
		View::get_partial('global/support');

		$rendered = true;

	}

	if ( is_tax() ) {

		global $product_query;

		$tax = get_queried_object();

		if ( $tax ) 

			$stack['archive_title'] = $tax->name;

			$query = [

				'taxonomy' => $tax->taxonomy,
				'field' => 'slug',
				'terms' => $tax->slug
				
			];

			$stack['items'] = (new Kaluna\ProductModel)->getAll(false, $query);
			$stack['filters'] = true;
			$stack['background'] = 'bg-black';
			$stack['grunge'] = 'grunge--white';
			$stack['tears'] = 'tear tear--blackUp tear--blackDown';
			$stack['query'] = $product_query;
			$stack['pagination'] = true;
			$stack['count'] = count($stack['items']);
			// $stack['alm'] = get_field('infinite_scroll', 'option') !== null && get_field('infinite_scroll', 'option') == true ? true : false;
			$stack['alm'] = false;
			$stack['content'] = $tax->description;

			View::get_partial('woo/archive', $stack);
			$rendered = true;

	}

	/**
	* bands
	*/

	if ( is_single() && get_post_type() == 'band' ) {

		View::get_partial('layouts/band');
		$rendered = true;

	}

	if ( is_archive() && get_post_type() == 'band' || is_page('bands') ) {

		global $band_query;

	    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	    if ( $paged == 1 ) {

			$stack['title'] = get_page_by_path( 'bands', ARRAY_A, 'page' ) ? get_page_by_path( 'bands', ARRAY_A, 'page' )['post_title'] : null;
			$stack['description'] = get_page_by_path( 'bands', ARRAY_A, 'page' ) ? get_page_by_path( 'bands', ARRAY_A, 'page' )['post_content'] : null;

		}
		
		$query['meta_key'] = 'alumni';
		$query['meta_value'] = 1;
		$query['meta_compare'] = '!=';

		$stack['bands'] = (new Kaluna\BandModel)->getAll(999, $query);
		$stack['query'] = $band_query;

		View::get_partial('layouts/bands', $stack);

		$rendered = true;

	}

	/**
	* pages
	*/

	if ( isset($post->ID) && get_post_type() == 'page' && !is_tax() && ( function_exists('is_woocommerce') && !is_woocommerce() ) ) {

		if ( (new Kaluna\ComponentModel)->exists($post->ID) ) {

			get_components_presenter($post->ID);
			$rendered = true;

		} else if ( Kaluna\PageModel::getTemplateSlug($post->ID) == 'page-faqs.php' ) { 

			get_faqs_presenter();
			$rendered = true;

		} else if ( Kaluna\PageModel::getTemplateSlug($post->ID) == 'page-history.php' ) { 

			get_history_presenter();
			$rendered = true;

		}

		if ( (new Kaluna\ComponentModel)->exists($post->ID) == false && $rendered == false ) {

			$data = [

				'title' => get_the_title(),
				'content' => get_the_content()

			];

			View::get_partial('layouts/content', $data);

		}

	}

}