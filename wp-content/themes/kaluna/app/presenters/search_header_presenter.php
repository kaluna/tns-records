<?php 

use Kaluna\boilerplate\View;

function get_search_header_presenter() {

	$title = false;
	$query = get_search_query();

	if ( $query == '' ) {
		
		$title = 'Search';

	} else {

		$title = 'You searched for: <mark>'.$query.'</mark>';

	}	

	$data = [

		'title' => $title,
		'query' => $query,

	];

	View::get_partial('global/search.header', $data);

}