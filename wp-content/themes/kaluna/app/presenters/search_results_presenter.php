<?php 

use Kaluna\boilerplate\View;

function get_search_results_presenter() {

	if ( get_search_query() != '' ) {

		$results = (new Kaluna\SearchModel)->get(get_search_query());
		$count = count($results);

		$status = null;

		if ( $count == 0 ) {

			$status = 'Your search for <mark>'.get_search_query().'</mark> return no results, try searching for something else.';

		}

		View::get_partial('global/search.results', ['status' => $status, 'results' => $results, 'count' => $count]);

	} else {

		View::get_partial('global/search.results.none');

	}

}