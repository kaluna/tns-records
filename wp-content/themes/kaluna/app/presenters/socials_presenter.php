<?php

use Kaluna\boilerplate\View;

function get_socials_presenter() {

	$networks = get_field('networks', 'option');
	$socials = [];

	if ($networks )

		foreach ($networks as $network) {

			if ( stristr($network['link']['url'], 'facebook') ): $icon = 'facebook'; endif;
			if ( stristr($network['link']['url'], 'twitter') ): $icon = 'twitter'; endif;
			if ( stristr($network['link']['url'], 'instagram') ): $icon = 'instagram'; endif;
			if ( stristr($network['link']['url'], 'youtube') ): $icon = 'youtube'; endif;
			if ( stristr($network['link']['url'], 'spotify') ): $icon = 'spotify'; endif;

			if ( $network['link'] !== 'n/a' || $network['link'] !== 'N/A' ) {

				$socials[] = [

					'icon' => $icon,
					'link' => $network['link']

				];

			}

		}

		View::get_partial('components/socials', $socials);

}