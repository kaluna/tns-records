<?php 

use Kaluna\boilerplate\View;

function get_title_stretch_presenter($title) {

	if (isset($title)) {

		View::get_partial('global/title-stretch', $title);

	}

}