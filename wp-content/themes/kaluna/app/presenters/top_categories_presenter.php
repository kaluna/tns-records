<?php 

use Kaluna\boilerplate\View;

function get_top_categories_presenter() {

	$stack = [];

	$stack['title'] = 'Popular categories';

	$categories = get_categories(['taxonomy' => 'product_cat']);
	usort($categories, 'sortByCount');

	$categories = array_slice($categories, 0, 6);

	foreach ($categories as $category) {

		if ( $category->name !== 'Uncategorized' ) {

			$link = get_term_link($category);

			$stack['items'][] = [

				'name' => $category->name,
				'url' => $link

			];

		}

	}

	View::get_partial('woo/list', $stack);

};