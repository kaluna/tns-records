<?php 

use Kaluna\boilerplate\View;

function get_woo_filters_presenter() {

	$location = get_queried_object();

	$filters = [];

	$product_categories = get_terms(['taxonomy' => ['product_cat', 'product_band'], 'parent' => 0]);

	if ( isset($location->parent) ) {

		$product_categories = get_terms(['taxonomy' => $location->taxonomy, 'parent' => $location->term_id]);
		
		$parent = get_term_by('id', $location->parent, $location->taxonomy);

		if ( $parent ) {

			$filters['back'] = [

				'name' => $parent->name,
				'id' => $parent->term_id,
				'url' => get_term_link($parent),

			];

		}

		if ( $location->parent == 0 ) {

			$filters['back'] = [

				'name' => get_the_title(wc_get_page_id('shop')),
				'url' => get_permalink(wc_get_page_id('shop')),

			];
			
		}

	}

	if ( empty($product_categories) ) {

		$product_categories = get_terms(['taxonomy' => $location->taxonomy, 'parent' => $location->parent]);

		$parent = get_term_by('id', $location->parent, $location->taxonomy);

		if ( $parent ) {

			$filters['back'] = [

				'name' => $parent->name,
				'id' => $parent->term_id,
				'url' => get_term_link($parent),

			];

		}

	} 

	if ( $product_categories ):

		foreach ($product_categories as $cat) {

			if ($cat->slug !== 'uncategorized') {

				$filters['terms'][$cat->taxonomy]['label'] = get_taxonomy($cat->taxonomy)->label; 

				if ( $cat->parent !== 0 ) {

					$filters['terms'][$cat->taxonomy]['label'] = get_term($cat->parent)->name; 

				}

				$filters['terms'][$cat->taxonomy]['items'][] = [

					'name' => $cat->name,
					'id' => $cat->term_id,
					'url' => get_term_link($cat),
					'active' => isset($location->term_id) && $location->term_id == $cat->term_id ? : false,
					'order' => $cat->term_order

				];

			}

		}
		
	endif;

	View::get_partial('woo/filters', $filters);

}