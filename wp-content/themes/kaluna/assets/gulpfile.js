// Set domain for browsersync to proxy
var domain = 'https://www.tnsrecords.local';

// Define plugins
var path = require('path');
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var notify = require('gulp-notify');
var autoprefixer = require('gulp-autoprefixer');
var cssnano = require('gulp-cssnano');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var svgstore = require('gulp-svgstore');
var svgmin = require('gulp-svgmin');
var babel = require('gulp-babel');

/**
* SVGs
*/ 

const SvgStore = () => {

    return gulp
    .src('svgs/*.svg')
    .pipe(svgmin(function (file) {
        var prefix = path.basename(file.relative, path.extname(file.relative));
        return {
            plugins: [{
                cleanupIDs: {
                    prefix: prefix + '-',
                    inlineSvg: true,
                    minify: true
                }
            
            }]
        }
    }))
    .pipe(svgstore())
    .pipe(gulp.dest('../dist'));

} 

/**
* scss
*/ 

const Scss = () => {

    return gulp.src('./scss/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass()
        .on('error', notify.onError({
            title: 'SASS Error',
            message: '<%= error.message %>'
        }))
    )
    .pipe(autoprefixer({overrideBrowserslist: ['last 3 versions', 'ie 9']}))
    .pipe(cssnano())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('../dist'))
    .pipe(browserSync.stream({match: './../style.css'}));

}

/**
* MinifyJs
*/ 

const MinifyJs = done => {
    return gulp.src([
        'node_modules/babel-polyfill/dist/polyfill.js',
        './js/*.js',
    ])
    .pipe(babel({
        presets: ["@babel/preset-env"]
    }))
    .pipe(uglify())
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest('../dist'));
    done();
}

/**
* BrowserSync
*/ 

const BrowserSync = () => { 
    browserSync.init({
        files: ['../dist/style.css'],
        proxy: domain,
        open: false,
        port: 3000,
        injectChanges: true,
        ghostMode: false,
        reloadDelay: 50
    });
}

/**
* Reload
*/ 

const Reload = () => {

    return browserSync.reload();

}

/**
* declare tasks
*/ 

gulp.task(SvgStore);
gulp.task(Scss);
gulp.task(MinifyJs);
gulp.task(BrowserSync);

/**
* Watch
*/ 

const Watch = () => {
    gulp.watch('./scss/*.scss', gulp.series('Scss'));
    gulp.watch('./scss/*/*.scss', gulp.series('Scss'));
    gulp.watch('./scss/style.scss', gulp.series('Scss'));
    gulp.watch('./js/*.js', gulp.series('MinifyJs'));
    gulp.watch([
        '*.php',
        '../views/*/*.php',
        '../*/*.php',
        './js/*.js'
    ]).on('change', Reload);
}

gulp.task(Watch);

// Provide default task for gulp
gulp.task('default', () => {
    gulp.start('Watch');
    gulp.start('MinifyJs');
});

/**
* do the tasks
*/ 

exports.default = gulp.series(
    gulp.parallel(SvgStore, Scss, MinifyJs, BrowserSync, Watch), 
);