window.addEventListener('load', function(){

	var script = document.createElement('script');

	script.onload = function () {
	    
	    window.cookieconsent.initialise({
		  "palette": {
		    "popup": {
		      "background": "#141414"
		    },
		    "button": {
		      "background": "#B32069",
		      "text": "#ffffff"
		    }
		  },
		  "content": {
		    "href": "/privacy-policy/"
		  }
		})

	};

	script.src = 'https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js';

	var link = document.createElement("link");
	link.href = "https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css";
	link.type = "text/css";
	link.rel = "stylesheet";
	link.media = "screen,print";

	document.head.appendChild(script);
	document.head.appendChild(link);

});