const faqTrigger = function() {

	const faqTriggers = document.querySelectorAll('.faqTrigger');
	const allContent = document.querySelectorAll('.faq');

	if ( faqTriggers ) {

		faqTriggers.forEach(trigger => {

			trigger.addEventListener('click', () => {

				const li = trigger.closest('li');

				if ( li.classList.contains('active') ) {

					trigger.setAttribute('aria-expanded', false);
					li.classList.remove('active');

				} else {

					trigger.setAttribute('aria-expanded', true);
					allContent.forEach(content => content.classList.remove('active'));
					li.classList.add('active');

				}

			})

		})

	}

}

if ( document.getElementById('faqList') ) {

	faqTrigger();

}
