const wooFilters = function(){

	const wooFilterArray = document.querySelectorAll('.wooFilter');

	if ( wooFilterArray ) {

		wooFilterArray.forEach(filter => {

			filter.addEventListener('change', () => {

				if (filter.value !== 'null') {

					window.location.href = filter.value;

				}

			})

		})

	}

};

if ( document.getElementById('wooFilters') ) {

	wooFilters();

}

/**
* .sortBy
*/

const sortBy = function(filter) {

	if ( filter ) {

		filter.addEventListener('change', () => {

			const url = new URL(window.location.href);
			url.searchParams.set('sortBy', filter.value);
			window.location.href = url.href;

		});

	}

}

const sortByFilter = document.getElementById('sortBy');

if ( sortByFilter ) {

	sortBy(sortByFilter);

}

