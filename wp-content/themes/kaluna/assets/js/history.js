const historyScroll = function(to) {

	let el = document.getElementById(to);
	let top = window.pageYOffset + el.getBoundingClientRect().top;
	let minus = '110';

	window.scrollTo({
		top: top - minus,
		left: 0,
		behavior: 'smooth'
	});

	let historyMobile = document.querySelector('.history--navigationMobile');
	historyMobile.classList.remove('active');

}

const historyNavigation = function(historyTriggers) {

	historyTriggers.forEach(trigger => {

		trigger.addEventListener('click', () => {

			historyScroll(trigger.getAttribute('data-id'));

		});

	})

}

const historyTriggers = document.querySelectorAll('.history--navigationTrigger');

if ( historyTriggers.length > 0 ) {

	historyNavigation(historyTriggers);

}

const historyNavigationSelect = function(historyNavigationSelectTrigger) { 

	historyNavigationSelectTrigger.addEventListener('change', () => {

		historyScroll(historyNavigationSelectTrigger.value);

	});

}

const historySelect = document.getElementById('historyNavigation');

if ( historySelect ) {

	historyNavigationSelect(historySelect);

}

const navigateToggle = document.getElementById('navigateToggle');

if ( navigateToggle ) {

	navigateToggle.addEventListener('click', () => {

		let historyMobile = document.querySelector('.history--navigationMobile');
		historyMobile.classList.toggle('active');

	})

}

/**
* active section
*/

const activeHistorySection = function() {

	const sections = document.querySelectorAll('body.history main section');	

	if ( sections.length > 0 ) {

		sections.forEach(section => {

			let historyMobile = document.querySelector('.history--navigation');

			if ( section.getBoundingClientRect().top < window.innerHeight ) {

				if ( section.classList.contains('bg-white') ) {

					historyMobile.classList.add('notWhite');

				} else {

					historyMobile.classList.remove('notWhite');

				}

			}

		});

	}


}

window.addEventListener('scroll', () => activeHistorySection());