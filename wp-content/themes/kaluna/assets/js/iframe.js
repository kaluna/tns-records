/**
* video aspect
*/ 

function iframe(){

	var videos = document.querySelectorAll('section.video, iframe.video[src*="youtube"], iframe[src*="youtube"], .video[src*="youtube"]');

	if ( videos.length > 0 ) {

		videos = Array.prototype.slice.call(videos);

		videos.forEach(function(video){

			let videoWidth = video.clientWidth;
			let videoHeight = videoWidth * 9/16;

			video.style.height = videoHeight + 'px';

		});

	}

	var bandcamp = document.querySelectorAll('iframe[src*="bandcamp.com"]');

	if ( bandcamp.length > 0 ) {

		bandcamp = Array.prototype.slice.call(bandcamp);

		bandcamp.forEach(function(player){

			let playerWidth = player.clientWidth;
			let playerHeight = playerWidth * 4/3;

			player.style.height = playerHeight + 'px';

		});

	}


}

iframe();

window.addEventListener('load',function(){

	iframe();

});

window.addEventListener('resize', function(){

	iframe();

});