const navToggle = function(){

	/**
	* toggle nav
	*/ 

	var nav = document.getElementById('mobileNavigation'),
		toggle_trigger = document.querySelectorAll('.nav--trigger');

	for (var i = toggle_trigger.length - 1; i >= 0; i--) {

		toggle_trigger[i].addEventListener('click', event => toggle());

	}

	function toggle() {

		event.preventDefault();

		if ( nav.classList.contains('active') ) {

			nav.classList.add('inactive');

			setTimeout(function(){

				nav.classList.remove('load', 'active', 'inactive');

			}, 250);

		} else {

			nav.classList.add('active');

		}

	}

	function close_nav() {

		if ( nav.classList.contains('active') ) {

			nav.classList.add('inactive');

			setTimeout(function(){

				nav.classList.remove('load', 'active', 'inactive');

			}, 250);

		}

	}

	/**
	* on esc press hide nav
	*/

	document.addEventListener('keydown',function(event){

	    if ( event.keyCode == 27 && window.innerWidth < 768 ) {

	         close_nav();

	    }

	}, false);

	/**
	* hide nav if opeon > tablet
	*/

	window.addEventListener("resize", function(){

		if ( window.innerWidth > 768 ){

	    	close_nav();

		}

	});

}; 

// init
navToggle();

const subMenu = function() {

	if ( window.innerWidth < 768 ) {

		let menuTriggers = document.querySelectorAll('.menu-item-has-children > a');

		if (menuTriggers.length > 0) {

			menuTriggers.forEach(trigger => {

				trigger.addEventListener('click', event => {

					event.preventDefault();

					trigger.parentNode.classList.toggle('active');

				});

			});

		}

	}

};

// init
subMenu();