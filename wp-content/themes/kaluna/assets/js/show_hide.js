const showHide = function(showHideTrigger) {

	showHideTrigger.forEach(trigger => {

		trigger.addEventListener('click', () => {

			const el = trigger.closest('section').querySelector('.show--hideContainer');

			if ( el ) {

				el.classList.toggle('active');

				let top = window.pageYOffset + el.getBoundingClientRect().top;
				let minus = '110';

				window.scrollTo({
					top: top - minus,
					left: 0,
					behavior: 'smooth'
				});

			}
  
		})

	})

};

const showHideTrigger = document.querySelectorAll('.showHide');

if ( showHideTrigger.length > 0 ) 
	showHide(showHideTrigger);