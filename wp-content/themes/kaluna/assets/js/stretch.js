const Stretch = function() {

	const stretch = document.querySelectorAll('.title--stretch');

	if ( stretch.length > 0 ) {

		stretch.forEach(title => {

			let size = title.innerHTML.replace(/ /g, "").length;

			if ( size > 6 && window.innerWidth > 420 ) {

				title.style.fontSize = 'calc(110vw / ' + size + ')';

			} else {

				title.style.fontSize = 'calc(110vw / 10)';

			}

		});

	}

}

Stretch();

window.addEventListener('resize', () => Stretch());
