const wcTabs = function() {

	const tabs = document.querySelectorAll('.woocommerce-tabs');

	// firsts

	if ( tabs.length > 0 ) {

		let firstTab = tabs[0].querySelectorAll('.wc-tabs li a')[0];
		firstTab.classList.add('active');

		let firstBlock = document.querySelectorAll('.woocommerce-Tabs-panel')[0];
		firstBlock.classList.add('active');

		tabs.forEach(tab => {

			const triggers = tab.querySelectorAll('.wc-tabs li a');
			
			triggers.forEach(trigger => {

				let tabName = trigger.href;

				trigger.addEventListener('click', event => {

					event.preventDefault();

					let tabBlock = document.getElementById(tabName.split('#')[1]);

					closeOpenTabs();

					trigger.classList.add('active');
					tabBlock.classList.add('active');

				});

			});

		});

	}

}	

const closeOpenTabs = function() {

	const tabs = document.querySelectorAll('.woocommerce-tabs');

	if ( tabs.length > 0 ) {

		tabs.forEach(tab => {

			let openTabs = document.querySelectorAll('.woocommerce-Tabs-panel.active, .wc-tabs li a.active');

			if ( openTabs )

				openTabs.forEach(tab => {

					tab.classList.remove('active');

				});

		});

	}

};

window.onload = wcTabs;