<section class="bg-black pdv--xl grunge--white tear tear--blackUp tear--blackDown">
	
	<div class="container">

		<div class="row justify-content-between">

			<div class="col-12 col-md-7">

				<?php if ($stack['title']): ?>

					<h2 class="ttu"><?php echo $stack['title'] ?></h2>
					
				<?php endif ?>
				
				<?php echo apply_filters('the_content', $stack['content']); ?>

			</div>

			<div class="col-12 col-md-4">
				
				<?php if ($stack['socials']): ?>

					<h3 class="mdb--l">Follow <?php echo get_the_title(); ?></h3>

					<ul class="band--socials nbs">
					
						<?php foreach ($stack['socials'] as $social): ?>

							<li><a class="font-heading text-white" href="<?php echo $social['url'] ?>" target="_blank" rel="nofollow"><svg class="mdr--s" viewBox="0 0 40 40"><use xlink:href="#<?php echo $social['slug'];?>" /></use></svg> <?php echo $social['name']; ?></a></li>
							
						<?php endforeach ?>

					</ul>

				<?php endif ?>

			</div>

		</div>

	</div>

</section>