<section class="alumni pdv--xl bg-white grunge--grey tear tear--whiteUp tear--whiteDown mdb--l">

	<div class="container">

		<h2 class="mdb--l">TNSrecords ALUMNI</h2>

		<?php if ($stack['bands']): ?>
			
			<div class="row">
				
				<?php foreach ($stack['bands'] as $band): ?>
					
					<div class="col-12 col-sm-6 col-md-3 mdb--n">
							
						<article class="item band alumni">

							<?php if (isset($band['images']) && $band['images']['square']['html']): ?>

								<div class="item--image bg-box mdb--m">

									<?php echo $band['images']['square']['html']; ?>

								</div>
								
							<?php endif ?>

							<?php if ($band['name']): ?>
								
								<h2 class="fz-l item--title">

									<?php echo $band['name']; ?> 

								</h2>

							<?php endif ?>

						</article>

					</div>
					
				<?php endforeach ?>

			</div>

		<?php endif ?>

	</div>

</section>
