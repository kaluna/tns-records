<?php use Kaluna\boilerplate\View; ?>

<?php if ($stack): ?>

	<section class="hero hero--band">
		
		<?php echo isset($stack['image']) ? $stack['image'] : false; ?>

		<div class="hero--wrapper">

			<div class="hero--content">
		
				<?php if ($stack['title']): ?>
					
					<p class="hero--title title--stretch font-heading text-white mdb--m"><?php echo $stack['title']; ?></p>

				<?php endif ?>

			</div>

		</div>

	</section>	

<?php endif ?>

