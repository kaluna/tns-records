<?php use Kaluna\boilerplate\View; ?>

<section class="pair pdb--xl tear tear--whiteUp tear--whiteDown grunge--grey bg--white">

	<div class="container">
			
		<div class="row justify-content-between flex-row-reverse">
			
			<div class="col-12 col-md-7 pair--content">

				<?php if ($stack['title']): ?>

					<h2 class="ttu"><?php echo $stack['title'] ?></h2>
					
				<?php endif ?>
				
				<?php echo apply_filters('the_content', $stack['content']); ?>

				<?php if ($stack['link']): ?>

					<?php View::get_partial('global/link', $stack['link']); ?>
					
				<?php endif ?>

			</div>

			<div class="col-12 col-md-5 pair--player">

				<?php // echo do_shortcode('[bandcamp album='..' size=large bgcol=ffffff linkcol=B41F6A tracklist=false]'); ?>

				<iframe src="https://bandcamp.com/EmbeddedPlayer/album=<?php echo $stack['bandcamp']['player']; ?>/size=large/bgcol=B41F6A/linkcol=0687f5/tracklist=false/transparent=true/" seamless></iframe>
				
			</div>

		</div>	

	</div>

</section>