<section class="videos pdv--xl bg--white grunge--pink tear tear--whiteDown">

	<div class="container container--sm">

		<h2 class="tac mdb--l ttu">Videos</h2>

		<div class="row justify-content-center">
	
			<?php foreach ($stack as $key => $item): ?>

				<?php if (count($stack) > 2 && $key === array_key_first($stack)): ?>

						<div class="col-12">
							
							<?php echo $item['video'] ?>

						</div>

					<?php else: ?>

						<div class="col-12 col-sm"><?php echo $item['video']; ?></div>
					
				<?php endif ?>
				
			<?php endforeach ?>

		</div>

	</div>

</section>