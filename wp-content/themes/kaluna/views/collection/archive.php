<?php 

use Kaluna\boilerplate\View;

if ($stack['posts']): ?>

	<?php if (isset($stack['title'])): ?>

		<?php get_title_stretch_presenter($stack['title']); ?>	
		
	<?php endif ?>

	<section class="posts pdv--xl bg-grey">

		<?php if ( isset($stack['alm']) && $stack['alm'] == true ) {

	    	get_alm_presenter($stack);

	    } else { ?>
		
			<div class="container">
				
				<div class="row">
					
					<?php foreach ($stack['posts'] as $post): ?>

						<div class="col-12 col-sm-6 col-xl-4">

							<?php View::get_partial('collection/detail', $post); ?>

						</div>
						
					<?php endforeach ?>

				</div>

			</div>

			<?php get_pagination_presenter($stack['query']); ?>

		<?php } ?>

	</section>

<?php endif ?>