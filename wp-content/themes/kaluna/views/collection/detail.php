<article class="post" data-category="<?php echo sanitize_title($stack['category']['name']); ?>">

	<?php if (isset($stack['category'])): ?>

		<div class="post--cat">
			
			<p class="font-heading fz-s mdb--b"><?php echo $stack['category']['name'] ?></p>

		</div>
		
	<?php endif ?>

	<?php if (isset($stack['images'])): ?>
		
		<div class="post--image">

			<a href="<?php echo $stack['url']; ?>" title="<?php echo $stack['name']; ?>">
			
				<?php echo $stack['images']['blog']['html']; ?>

			</a>

		</div>

	<?php endif ?>
	
	<h3 class="post--name"><a href="<?php echo $stack['url']; ?>" title="<?php echo $stack['name']; ?>"><?php echo $stack['name']; ?></a></h3>

</article>