<article class="item <?php echo get_post_type($stack['id']); ?>">

	<?php if (isset($stack['images']) && $stack['images']['square']['html']): ?>

		<div class="item--image shake mdb--s">

			<a class="item--anchor" href="<?php echo $stack['url']; ?>" title="<?php echo $stack['name']; ?>">

				<?php echo $stack['images']['square']['html']; ?>

			</a>

		</div>
		
	<?php endif ?>

	<?php if ($stack['name']): ?>

		<div class="item--data">
		
			<h2 class="fz-l item--title mdb--s">

				<a href="<?php echo $stack['url']; ?>" title="<?php echo $stack['name']; ?>">

					<?php echo $stack['name']; ?> 

				</a>

			</h2>

			<?php if (get_post_type($stack['id']) == 'product'): ?>

				<div class="mdt--s row justify-content-between align-items-center item--actions">

					<?php if (isset($stack['price'])): ?>
						
						<p class="col-12 col-xl-auto fw--bold woocommerce-Price-amount mdb--s">

							<a href="<?php echo $stack['url']; ?>" title="<?php echo $stack['name']; ?>"><?php echo $stack['price']; ?></a>
							
						</p>

					<?php endif; ?>

					<div class="col-12 col-xl-auto item--button mdb--s">
						
						<a class="button small" href="<?php echo $stack['url']; ?>" title="<?php echo $stack['name']; ?>"><?php echo isset($stack['link']['text']) ? $stack['link']['text'] : 'View product' ?></a>

					</div>

				</div>
				
			<?php endif ?>

		</div>

	<?php endif ?>

</article>