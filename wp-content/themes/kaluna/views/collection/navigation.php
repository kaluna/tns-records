<section class="single--navigation pdb--l mdb--m">

	<div class="container">
	
		<div class="row justify-content-center">
			
			<div class="col-auto"><?php previous_post_link(); ?></div>
			<div class="col-auto"><?php next_post_link(); ?></div>

		</div>

	</div>

</section>