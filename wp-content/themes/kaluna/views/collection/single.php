<section class="post bg-grey pdt--xl pdb--l">
	
	<div class="container container--sm">
		
		<h1 class="mdb--n"><?php echo $stack['name']; ?></h1>

		<ul class="mdv--s post--meta row nbs align-items-center">
			
			<li class="col-auto mdb--n"><p class="mdb--n"><?php echo $stack['posted_on'] ?></p></li>

			<li class="col-auto mdb--n"><a href="<?php echo $stack['category']['url'] ?>"><?php echo $stack['category']['name'] ?></a></li>

			<li class="col-auto mdb--n"><?php echo do_shortcode('[jetpack_share]'); ?></li>
	
		</ul>

		<div class="post--image mdt--l mdb--m">

			<div class="bg-box">

				<?php echo $stack['images']['feature']['html']; ?>

			</div>

		</div>

	</div>

</section>

<section class="post--content pdv--l bg-white grunge--grey tear tear--whiteUp tear--whiteDown">

	<div class="container container--sm">
		
		<?php echo apply_filters('apply_filters', $stack['content']); ?>

	</div>
	
</section>

<?php get_single_navigation(); ?>