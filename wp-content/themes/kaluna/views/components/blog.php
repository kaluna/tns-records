<?php use Kaluna\boilerplate\View; ?>

<section class="blog pdv--xl bg-grey tear tear--newsUp tear--newsDown">
	
	<div class="container">

		<?php if ($stack['title']): ?>

			<h2><?php echo $stack['title']; ?></h2>
			
		<?php endif ?>
		
		<div class="row">
			
			<?php foreach ($stack['posts'] as $post): ?>

				<div class="col-12 col-sm-6 col-md-4">

					<?php View::get_partial('collection/detail', $post); ?>

				</div>
				
			<?php endforeach ?>

		</div>

		<?php if (isset($stack['page_for_posts'])): ?>

			<div class="tac mdt--m">
				
				<a class="button" href="<?php echo $stack['page_for_posts'] ?>" title="See more TNSrecords related news, interviews and more" aria-label="See more TNSrecords related news, interviews and more">Read more TNS news</a>

			</div>
			
		<?php endif ?>

	</div>

</section>