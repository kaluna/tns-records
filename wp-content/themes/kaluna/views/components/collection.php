<?php

use Kaluna\boilerplate\View; 

$size = 'col-lg-3';

$count = isset($stack['items']) ? count($stack['items']) : 0;

if ( $count % 3 == 0 ) {

	$size = 'col-lg-4';

} ?>

<section class="collection pdv--l <?php echo isset($stack['tears']) ? $stack['tears'] . ' tear': false; ?> <?php echo isset($stack['grunge']) ? $stack['grunge'] : false; ?> <?php echo isset($stack['background']) ? $stack['background'] : false; ?>">
	
	<?php if (isset($stack['title'])): ?>

		<h2 class="tac"><?php echo $stack['title']; ?></h2>
		
	<?php endif ?>

		<?php if (isset($stack['filters']) && $stack['filters'] == true): ?>

			<div class="container container--lg">

				<div class="row">

					<div class="sidebar woo--sidebar col-12 col-md-3">

						<aside>

							<?php get_woo_filters_presenter(); ?>

						</aside>

					</div>

					<div class="col-12 col-md-9">
						
						<?php if ( isset($stack['alm']) && $stack['alm'] == true ) {

					    	get_alm_presenter($stack);

					    } else { ?>

						<?php if ($stack['items']): ?>

							<div class="row align-content-start">
							
								<?php foreach ($stack['items'] as $item): ?>

									<div class="col-12 col-sm-6 <?php echo $size; ?>">

										<?php View::get_partial('collection/index', $item); ?>

									</div>
									
								<?php endforeach ?>

							</div>

							<?php endif ?>

						<?php } ?>

					</div>

				</div>

			</div>

			<?php else: ?>
	
				<div class="container">

					<?php if ( isset($stack['alm']) && $stack['alm'] == true ) {

				    	get_alm_presenter($stack);

				    } else { ?>

					<?php if ($stack['items']): ?>

						<div class="row align-content-start mdt--l">
						
							<?php foreach ($stack['items'] as $item): ?>

								<div class="item--holder col-12 col-sm-6 <?php echo $size; ?>">

									<?php View::get_partial('collection/index', $item); ?>

								</div>
								
							<?php endforeach ?>

						</div>

						<?php endif ?>

					<?php } ?>

				</div>

		<?php endif ?>

		<?php if (isset($stack['link'])): ?>

			<div class="mdt--m tac">
					
				<?php View::get_partial('global/link', $stack['link']); ?>

			</div>

		<?php endif ?>

	</div>

	<?php if ( ( (isset($stack['pagination']) && $stack['pagination'] == true ) && (isset($stack['query']->max_num_pages) && $stack['query']->max_num_pages > 1)) && (isset($stack['alm']) && $stack['alm'] == false) ): ?>

		<?php get_pagination_presenter($stack['query']); ?>
		
	<?php endif ?>

</section>