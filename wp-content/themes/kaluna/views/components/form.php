<section class="form pdv--xl <?php echo isset($stack['tears']) ? $stack['tears'] . ' tear': false; ?> <?php echo isset($stack['grunge']) ? $stack['grunge'] : false; ?> <?php echo isset($stack['background']) ? $stack['background'] : false; ?>">
	
	<div class="container container--xs">
		
		<?php if ($stack['title']): ?>

			<h2><?php echo $stack['title']; ?></h2>
			
		<?php endif ?>

		<?php if ($stack['introduction']): ?>
			
			<?php echo apply_filters('the_content', $stack['introduction']); ?>

		<?php endif ?>

		<?php if ($stack['form_id']): ?>

			<?php echo do_shortcode('[gravityform id="'.$stack['form_id'].'" title="false" description="false" ajax="true"]'); ?>
			
		<?php endif ?>

	</div>

</section>