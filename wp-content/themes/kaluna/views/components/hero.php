<?php use Kaluna\boilerplate\View; ?>

<?php if ($stack): ?>

	<section class="hero">
		
		<?php echo isset($stack['image']) ? $stack['image'] : false; ?>

		<?php if ( $stack['title'] && ( $stack['text_position'] == null || $stack['text_position'] !== null && $stack['text_position'] !== 'bottom') ): ?>

			<div class="hero--wrapper">

				<div class="container">

					<div class="hero--content">
				
						<?php if ($stack['title']): ?>
							
							<p class="hero--title <?php echo isset($stack['text_color']) ? $stack['text_color'] : null; ?> font-heading mdb--m"><?php echo $stack['title']; ?></p>

						<?php endif ?>
						
						<?php if ($stack['sub_title']): ?>
							
							<p class="hero--subTitle <?php echo isset($stack['text_color']) ? $stack['text_color'] : null; ?> font-heading ttu mdb--n"><?php echo $stack['sub_title']; ?></p>

						<?php endif ?>
						
						<?php if ($stack['link']): ?>

							<div class="hero--cta mdt--m">

								<?php View::get_partial('global/link', $stack['link']); ?>

							</div>

						<?php endif ?>

					</div>

				</div>

			</div>

		<?php else: ?>

			<p class="hero--title hero--titleBottom title--stretch font-heading text-black mdb--m"><?php echo $stack['title']; ?></p>
			
		<?php endif ?>

	</section>	

<?php endif ?>

