<section class="pagination pdt--l">
	
	<div class="container tac">
			
		<p class="font-heading ttu fw-bold">Navigation</p>

		<?php echo paginate_links(

			[

		        'base'       => str_replace($stack['big'], '%#%', esc_url(get_pagenum_link( $stack['big'] ))),
		        'format'     => '?paged=%#%',
		        'current'    => max( 1, $stack['paged'] ),
		        'total'      => $stack['max_page'],
		        'mid_size'   => 1,
		        'prev_text'  => __('«'),
		        'next_text'  => __('»'),
		        'type'       => 'list'

	   		]

		); ?>

	</div>

</section>