<?php use Kaluna\boilerplate\View; ?>

<section class="pair pdv--xl <?php echo isset($stack['tears']) ? $stack['tears'] . ' tear': false; ?> <?php echo isset($stack['grunge']) ? $stack['grunge'] : false; ?> <?php echo isset($stack['background']) ? $stack['background'] : false; ?>">

	<div class="container">
			
		<div class="row justify-content-between <?php echo $stack['alignment'] == 'alternative' ? 'flex-row-reverse' : null; ?>">
			
			<div class="col-12 col-md-6 pair--content">
				
				<?php echo apply_filters('the_content', $stack['content']); ?>

				<?php if ($stack['link']): ?>

					<?php View::get_partial('global/link', $stack['link']); ?>
					
				<?php endif ?>

			</div>

			<div class="col-12 col-md-5 pair--image">
				
				<?php echo $stack['image']; ?>

			</div>

		</div>	

	</div>

</section>