<?php use Kaluna\boilerplate\View; ?>

<section class="resources bg-white pdv--xl tear tear--whiteUp tear--whiteDown grunge--grey">
	
	<div class="container pdb--m">
		
		<h1 class="tac mdb--l ttu">Resources</h1>

		<?php foreach ($stack as $section): ?>

			<section class="resources--section">

				<h2><?php echo $section['title'] ?></h2>

				<div class="row">

					<?php foreach ($section['links'] as $link): ?>

						<article class="resource col-12 col-md-6 col-lg-3 mdb--m">

							<h3><?php echo $link['title'] ?></h3>
							<?php echo apply_filters('the_content', $link['description']); ?>

							<?php View::get_partial('global/link', ['title' => $link['link']['title'], 'url' => $link['link']['url'], 'type' => 'blank', 'target' => '_blank'] ); ?>

						</article>
						
					<?php endforeach ?>

				</div>

			</section>
			
		<?php endforeach ?>

	</div>

</section>