<section class="signup bg-pink grunge--grey pdv--l tear tear--pinkUp tear--pinkDown">
	
	<div class="container">

		<h2><?php echo $stack['title']; ?></h2>

		<div class="text-heading fw-bold fz-l"><?php echo apply_filters('the_content', $stack['description']); ?></div>
		
		<?php echo do_shortcode('[mc4wp_form id="'.$stack['form_id'].'"]'); ?>

	</div>

</section>