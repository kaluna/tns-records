<?php if ($stack): ?>

	<div class="social--networks">
		
		<ul>
				
			<?php foreach ($stack as $social): ?>

				<li class="mdb--s">
					
					<a class="font-heading ttu" href="<?php echo $social['link']['url'] ?>" target="<?php echo $social['link']['target'] ? $social['link']['target'] : false; ?>" <?php echo $social['link']['target'] == '_blank' ? 'rel="noopener"' : null; ?>>
						
						<svg class="<?php echo $social['icon'] ?>" viewBox="0 0 40 40"><use xlink:href="#<?php echo $social['icon'] ?>" /></use></svg>

						<span class="mdl--xs"><?php echo $social['link']['title']; ?></span>

					</a>

				</li>
				
			<?php endforeach ?>

		</ul>

	</div>	

<?php endif ?>

