<section class="statement bg-white grunge-grey tear tear--pinkDown pdt--xl">

	<div class="statement--inner bg-pink grunge--grey tear tear--pinkUp tear--pinkDown">
	
		<div class="container">
			
			<div class="row no-gutters justify-content-center">

				<?php if ($stack['image']): ?>

					<div class="statement--image mdb--l">

						<?php echo $stack['image']; ?>

					</div>
					
				<?php endif ?>
				
				<div class="col-12 col-md-8">

					<?php echo apply_filters('the_content', $stack['content']); ?>

				</div>

			</div>

		</div>

	</div>

</section>