<section class="textarea bg-white grunge--grey tear tear--whiteUp tear--whiteDown pdv--xl">

	<div class="container">
		
		<div class="row">
			
			<div class="col-12 col-lg-9 fz-l">
				
				<?php echo apply_filters('the_content', $stack['content']); ?>

			</div>

		</div>

	</div>

	<?php if ($stack['stroke'] == true): ?>

		<div class="stroke row no-gutters justify-content-center pdt--l">

			<div class="col-6 mdb--n">
			
				<img src="<?php echo get_resource('misc/stroke.svg'); ?>" role="presentation">

			</div>

		</div>
		
	<?php endif ?>

</section>