<section class="triplet bg-white grunge--grey pdv--xl">
	
	<div class="container">

		<h2 class="tac"><?php echo $stack['title']; ?></h2>
		
		<div class="row">
			
			<?php foreach ($stack['items'] as $item): ?>

				<div class="col-12 col-md-4">
					
					<div class="triplet--item pdh--s">
						
						<div class="triplet--item__image mdb--m">

							<?php echo $item['image']; ?>
								
						</div>

						<?php echo apply_filters('the_content', $item['content']); ?>

					</div>

				</div>
				
			<?php endforeach ?>

		</div>

	</div>

</section>