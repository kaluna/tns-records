<footer class="pdv--l bg-black tear tear--blackUp grunge--pink">

	<div class="container">
	
		<div class="row">
			
			<div class="col-12 col-md-6 col-lg-4 footer--sitemap">

				<p class="font-heading text-white fz-xl ttu fw-bold">Sitemap</p>
				
				<?php get_menu_presenter(['theme_location' => 'footer'], 'footerNavigation'); ?>

			</div>

			<div class="col-12 col-md-6 col-lg-4 footer--social">

				<p class="font-heading text-white fz-xl ttu fw-bold">Social</p>

				<?php get_socials_presenter(); ?>

			</div>

			<div class="col-12 col-md-6 col-lg-4 footer--statement">

				<img src="<?php echo get_resource('logo/tns-logo.svg'); ?>" alt="TNS Records logo" class="footer--logo tns--logo mdb--m">
				
				<p class="font-heading fz-s text-white mdb--n">&copy; <?php echo get_bloginfo('name'); ?> <?php echo date('Y'); ?> | Built by <a href="https://www.kaluna.co.uk" target="_blank" rel="nofollow noreferrer">Kaluna</a></p>

			</div>

		</div>

	</div>

</footer>

</body>

<?php wp_footer(); ?>