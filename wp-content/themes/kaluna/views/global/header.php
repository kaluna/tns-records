<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1.0">
<link rel="preload" href="https://use.typekit.net/hwd2smc.css" as="style">
<link rel="favicon" href="<?php echo get_bloginfo('url') . '/favicon.ico'; ?>" />
<link rel="icon" href="<?php echo get_bloginfo('url') . '/favicon.ico'; ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" href="https://use.typekit.net/hwd2smc.css">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-17059961-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-17059961-1');
</script>

<?php wp_head(); ?>

</head>

<body <?php body_class((new Kaluna\PostModel)::getSlug()); ?>>

<?php get_menu_presenter(['theme_location' => 'mobile'], 'mobileNavigation', 'bg-black grunge--pink'); ?>
	
<header class="masthead pdb--s mdb--n bg-black tear tear--blackDown" id="masthead">

	<div class="container">
		
		<div class="row mdb--n justify-content-between">
			
			<div class="masthead--logo">

				<?php if (is_cart() || is_checkout()): ?>
						
					<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" title="<?php echo get_bloginfo('name') ?> Home">
						
						<img src="<?php echo get_resource('logo/tns-logo.svg'); ?>" alt="TNS Records logo" class="footer--logo tns--logo">

					</a>

				<?php else: ?>

					<a href="<?php echo get_bloginfo('url') ?>" title="<?php echo get_bloginfo('name') ?> Home">
						
						<img src="<?php echo get_resource('logo/tns-logo.svg'); ?>" alt="TNS Records logo" class="footer--logo tns--logo">

					</a>

				<?php endif ?>

			</div>
			
			<div class="col-auto col-lg-4">

			</div>

			<button aria-label="Open Mobile Navigation" class="nav--trigger blank"><svg class="bars" viewBox="0 0 20 20"><use xlink:href="#bars" /></use></svg></button>
	
			<div class="col-12 col-md header--right mdb--n">

				<?php get_menu_presenter(['theme_location' => 'top', 'menu_class' => 'row justify-content-end align-items-center'], 'topNavigation'); ?>

			</div>

		</div>

	</div>

</header>