<nav class="<?php echo isset($stack['class']) ? $stack['class'] : false; ?>" id="<?php echo isset($stack['id']) ? $stack['id'] : false; ?>">

	<?php if ($stack['args']['theme_location'] == 'mobile'): ?>

		<div class="nav--logo mdt--m">
		
			<a href="/" title="<?php echo get_bloginfo('name') ?> Home">
				
				<img src="<?php echo get_resource('logo/tns-logo.svg'); ?>" alt="TNS Records logo" class="footer--logo tns--logo">

			</a>

			<button class="nav--trigger close blank"><svg class="close" viewBox="0 0 20 20"><use xlink:href="#close" /></use></svg></button>

		</div>

	<?php endif ?>
	
	<?php wp_nav_menu($stack['args']); ?>

</nav>