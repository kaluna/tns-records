<section class="search bg-black pdv--xl grunge--pink tear tear--blackUp tear--blackDown">

	<div class="container">

		<h1 class="mdb--n"><?php echo $stack['title']; ?></h1>

		<div class="search--form mdt--l">
			
			<?php get_search_form(); ?>

		</div>

	</div>

</section>
