<?php use Kaluna\boilerplate\View; ?>

<section class="search--results bg-white grunge--grey pdv--xl">
	
	<div class="container">
		
		<h4 class="mdb--m">Enter your search term above, or try popular searches below: </h4>

		<div class="row">

			<div class="col-12 col-lg-8">

				<div class="row">
			
					<div class="col-6 col-md-4"><a href="<?php get_bloginfo('url'); ?>/?s=TNS Records">TNS Records</a></div>
					<div class="col-6 col-md-4"><a href="<?php get_bloginfo('url'); ?>/?s=Faintest Idea">Faintest Idea</a></div>
					<div class="col-6 col-md-4"><a href="<?php get_bloginfo('url'); ?>/?s=Revenge">Revenge</a></div>
					<div class="col-6 col-md-4"><a href="<?php get_bloginfo('url'); ?>/?s=Vinyl">Vinyl</a></div>
					<div class="col-6 col-md-4"><a href="<?php get_bloginfo('url'); ?>/?s=New releases">New releases</a></div>
					<div class="col-6 col-md-4"><a href="<?php get_bloginfo('url'); ?>/?s=Manchester Punk Festival">Manchester Punk Festival</a></div>

				</div>

			</div>

		</div>

	</div>

</section>