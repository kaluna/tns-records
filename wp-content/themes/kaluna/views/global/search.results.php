<?php use Kaluna\boilerplate\View; ?>

<section class="search--results bg-white grunge--grey pdv--xl">
	
	<div class="container">
		
		<h2><?php echo $stack['status']; ?></h2>

		<?php if ($stack['count'] > 0): ?>

			<?php foreach ($stack['results'] as $post_type => $results): ?>

				<div class="search--results__section">

					<h2><?php echo get_post_type_object($post_type)->labels->name; ?></h2>

					<ul class="search--results__list nbs">
					
						<?php foreach ($results as $result): ?>

							<li>

								<?php View::get_partial('collection/search', $result); ?>

							</li>
												
						<?php endforeach ?>

					</ul>

				</div>
				
			<?php endforeach ?>
			
		<?php endif ?>

	</div>

</section>