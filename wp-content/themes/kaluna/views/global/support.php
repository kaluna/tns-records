<section class="support bg-white grunge--grey pdv--xl tac">

	<div class="container pdv--m">
	
		<p class="ttu fz-xl font--bold font-heading mdb--n">Need help? Read our <a href="/faqs">FAQs</a>.</p>

	</div>

</section>