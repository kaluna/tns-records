<ul class="history--navigation">
	
	<?php foreach ($stack as $item): ?>

		<?php if (isset($item['slug'])): ?>

			<li>

				<button class="blank history--navigationTrigger fz-s" type="button" title="Scroll to <?php echo $item['title']; ?>" data-id="<?php echo $item['slug']; ?>">

					<div><?php echo $item['title']; ?></div>
						
				</button>

			</li>
				
		<?php endif ?>
		
	<?php endforeach ?>

</ul>

<div class="history--navigationMobile pdv--m bg--white grunge--grey">

	<div class="container">

		<button type="button" class="button small mdb--xs" id="navigateToggle">Navigate</button>

		<select class="small" name="historyNavigation" id="historyNavigation">
			
			<?php foreach ($stack as $item): ?>

				<?php if (isset($item['slug'])): ?>

					<option value="<?php echo $item['slug']; ?>"><?php echo $item['title']; ?></option>

				<?php endif ?>

			<?php endforeach ?>

		</select>

	</div>

</div>