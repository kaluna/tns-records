<section id="<?php echo $stack['id']; ?>" class="quote pdv--xl bg-white grunge--pink tear tear--whiteUp tear--whiteDown">

	<div class="container">

		<<?php echo $stack['quote']['tag']; ?> class="headline--text">
	
			<?php echo $stack['quote']['text'] ?>

			<div class="mdt--s fz-l">&mdash; <?php echo $stack['quote']['author'] ?></div>

		</<?php echo $stack['quote']['tag']; ?>>

	</div>

</section>	