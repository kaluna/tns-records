<section id="<?php echo $stack['id']; ?>" class="section--history pdv--l <?php echo isset($stack['tears']) ? $stack['tears'] . ' tear': false; ?> <?php echo isset($stack['grunge']) ? $stack['grunge'] : false; ?> <?php echo isset($stack['background']) ? $stack['background'] : false; ?>">
	
	<div class="container <?php echo $stack['show_hide'] == true ? 'show--hideContainer' : null; ?>">
		
		<?php if (isset($stack['title'])): ?>

			<<?php echo $stack['title']['tag']; ?>>
			
				<?php echo $stack['title']['title'] ?>

			</<?php echo $stack['title']['tag']; ?>>
			
		<?php endif ?>

		<?php if ($stack['image']['type'] == 'full'): ?>

			<div class="section--image mdb--m"><?php echo getImage($stack['image']['image']['ID'], 'full', 'html', false, true); ?></div>
			
		<?php endif ?>

		<?php if ($stack['introduction']): ?>

			<div class="section--introduction">

				<?php echo apply_filters('the_content', $stack['introduction']); ?>

				<?php if ($stack['show_hide'] == true): ?>
					
					<button type="button" class="button showHide readMore">Read more</button>

				<?php endif ?>

			</div>
			
		<?php endif ?>

		<?php if ($stack['content']): ?>

			<div class="section--content <?php echo $stack['show_hide'] == true ? 'show--hide' : null; ?> <?php echo $stack['image']['type'] == 'indented'; ?>">

				<?php if ($stack['image']['type'] == 'indented'): ?>

					<div class="section--image indented mdb--m"><?php echo getImage($stack['image']['image']['ID'], 'full', 'html', false, true); ?></div>
					
				<?php endif ?>

				<?php echo apply_filters('the_content', $stack['content']); ?>

				<?php if ($stack['show_hide'] == true): ?>
					
					<button type="button" class="button showHide">Read less</button>

				<?php endif ?>

			</div>
			
		<?php endif ?>

	</div>

</section>