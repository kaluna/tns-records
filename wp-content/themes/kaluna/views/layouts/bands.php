<?php use Kaluna\boilerplate\View; ?>

<?php if (isset($stack['title'])): ?>

	<?php get_title_stretch_presenter($stack['title']); ?>	
	
<?php endif ?>

<section class="bands pdv--xl bg-black tear tear--blackUp tear--blackDown grunge--pink">

	<div class="container container--sm">

		<?php if (isset($stack['description'])): ?>

			<?php echo apply_filters('the_content', $stack['description']) ?>
			
		<?php endif ?>

	</div>

	<div class="container">
	
		<div class="row mdt--l">
		
			<?php foreach ($stack['bands'] as $item): ?>

				<div class="col-12 col-md-4">

					<?php View::get_partial('collection/index', $item); ?>

				</div>
				
			<?php endforeach; ?>

		</div>

	</div>

	<?php get_pagination_presenter($stack['query']); ?>

</section>
			
<?php get_band_alumni_presenter(); ?>
