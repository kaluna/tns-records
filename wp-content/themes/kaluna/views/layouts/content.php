<?php get_title_stretch_presenter($stack['title']); ?>	

<?php if ($stack['content']): ?>

	<section class="pdv--l bg-black tear tear--blackUp tear--blackDown grunge--white">
		
		<div class="container container--sm">
			
			<?php echo apply_filters('the_content', $stack['content']); ?>

		</div>

	</section>

<?php endif ?>