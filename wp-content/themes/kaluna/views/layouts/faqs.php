<section class="faqs pdv--xl mdb--l">
	
	<div class="container container--sm">
		
		<?php if ($stack): ?>

			<ul class="nbs faqs" id="faqList" data-allow-toggle>
			
				<?php foreach ($stack as $faq): ?>

					<li class="faq" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
							
						<h2><button itemprop="name" class="blank faqTrigger" id="<?php echo sanitize_title($faq['question']) ?>" aria-controls="<?php echo sanitize_title($faq['question']) ?>__content"><svg class="plus" viewBox="0 0 20 20"><use xlink:href="#plus" /></use></svg> <?php echo $faq['question'] ?></button></h2>

						<div class="faq--content" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer" role="region" aria-labelledby="<?php echo sanitize_title($faq['question']) ?>" id="<?php echo sanitize_title($faq['question']) ?>__content">

							<div itemprop="text">
							
								<?php echo apply_filters('the_content', $faq['answer']); ?>

							</div>

						</div>

					</li>
					
				<?php endforeach ?>

			</ul>

		<?php endif ?>

	</div>

</section>