<?php 

use Kaluna\Boilerplate\View; ?>

<section class="product archive">

	<?php View::get_partial('layouts/content', [

		'title' => $stack['archive_title'], 
		'content' => $stack['content']

	]); ?>

	<?php View::get_partial('components/collection', $stack); ?>

	<?php get_store_footer_presenter(); ?>

</section>