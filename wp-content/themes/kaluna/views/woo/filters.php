<section class="filters mdb--l" id="wooFilters">

	<div class="filters--section mdb--l">
		
		<p class="text-white font-heading fz-l ttu mdb--m">Sort by:</p>

		<select name="sortBy" id="sortBy">

			<option <?php echo isset($_GET['sortBy']) && $_GET['sortBy'] == 'latest' ? 'selected' : null; ?> value="latest">Sort by latest</option>
			<option <?php echo isset($_GET['sortBy']) && $_GET['sortBy'] == 'price_desc' ? 'selected' : null; ?> value="price_desc">Sort by: Price (high to low)</option>
			<option <?php echo isset($_GET['sortBy']) && $_GET['sortBy'] == 'price_asc' ? 'selected' : null; ?> value="price_asc">Sort by: Price (low to high)</option>

		</select>

	</div>

	<div class="filters--section">
		
		<p class="text-white font-heading fz-l ttu mdb--m">Browse by:</p>

		<?php if ($stack): ?>

			<?php if (isset($stack['back'])): ?>

				<div class="mdb--m">

					<a class="text-white" href="<?php echo $stack['back']['url'] ?>">&larr; Back to <?php echo $stack['back']['name'] ?></a>

				</div>

			<?php endif ?>

			<?php if (isset($stack['terms'])): ?>
				
				<?php foreach ($stack['terms'] as $tax => $term): ?>

					<select class="wooFilter" name="<?php echo $tax; ?>" id="<?php echo $tax; ?>">
						
						<option value="null"><?php echo $term['label']; ?></option>

						<?php if ( $term['label'] !== 'TNS Bands' ): usort($term['items'], 'sortByOrder'); endif; ?>

						<?php foreach ($term['items'] as $cat): ?>

							<option <?php echo $cat['active'] == true ? 'selected' : false; ?> value="<?php echo $cat['url'] ?>"><?php echo $cat['name']; ?></option>
							
						<?php endforeach ?>

					</select>

				<?php endforeach ?>

			<?php endif; ?>

		<?php endif ?>

	</div>

</section>