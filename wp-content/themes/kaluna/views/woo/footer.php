<section class="woocommerce--footer pdv--xl bg--white grunge--grey tear tear--whiteUp tear--whiteDown">

	<div class="container">

		<div class="row">
			
			<div class="col-12 col-md-4">
				
				<?php get_latest_product_presenter(); ?>

			</div>
			
			<div class="col-12 col-md-4">
				
				<?php get_best_sellers_presenter(); ?>

			</div>
			
			<div class="col-12 col-md-4">
				
				<?php get_featured_products_presenter(); ?>

			</div>

		</div>		

	</div>

</section>