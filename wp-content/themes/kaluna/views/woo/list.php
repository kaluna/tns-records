<section class="list woo--list">

	<h3 class="mdb--m"><?php echo $stack['title']; ?></h3>
	
	<?php if ($stack['items']): ?>

		<ul class="nbs">
			
			<?php foreach ($stack['items'] as $item): ?>

				<li class="row align-items-center">

					<?php if (isset($item['images']['square']) && $item['images']['square']['html']): ?>

						<div class="col-3">

							<div class="bg-box">
						
								<a href="<?php echo $item['url']; ?>" title="<?php echo $item['name']; ?>">

									<?php echo $item['images']['square']['html'] ?>

								</a>

							</div>

						</div>
						
					<?php endif ?>

					<div class="col">
					
						<a href="<?php echo $item['url']; ?>" title="<?php echo $item['name']; ?>"><?php echo $item['name']; ?></a>

					</div>

				</li>
				
			<?php endforeach ?>

		</ul>
		
	<?php endif ?>

</section>