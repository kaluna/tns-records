<?php

use Kaluna\boilerplate\View;

global $product; 
$post = (new Kaluna\ProductModel)->get($product->get_id()); ?>

<li <?php wc_product_class( 'col-6 col-md-3', $product ); ?>>
	
	<?php View::get_partial('collection/index', $post); ?>

</li>